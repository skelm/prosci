#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Common functions shared by all modules in prosci
#
# Author: Sebastian Kelm
# Created: 12/06/2009
#
from __future__ import print_function

import os
import sys
import subprocess
import shlex
import string
import collections
import re
from bisect import bisect_left
from multiprocessing import cpu_count, Pool


# Python3 removed the built-in cmp function
try:
  cmp(1, 2)
except NameError:
  def cmp(a, b):
    return (a>b)-(a<b)


class OverridableField(object):
  "Descriptor to access a fields that should be the same for all contents of the owner object"

  def __init__(self, field):
    self.f = field

  def __get__(self, obj, cls=None):
      for item in obj:
        if item is not None:
          return getattr(item, self.f)
      return None


class SettableField(object):
  "Descriptor to access a fields that should be the same for all contents of the owner object"

  def __init__(self, field):
    self.f = field

  def __get__(self, obj, cls=None):
      for item in obj:
        if item is not None:
          return getattr(item, self.f)
      return None

  def __set__(self, obj, val):
      for item in obj:
        if item is not None:
          setattr(item, self.f, val)

  def __delete__(self, obj):
      raise AttributeError("Illegal operation: cannot delete attribute")


class ReadOnlyField(object):
  "Descriptor to access (read-only) a fields that should be the same for all contents of the owner object"

  def __init__(self, field):
    self.f = field

  def __get__(self, obj, cls=None):
      for item in obj:
        if item is not None:
          return getattr(item, self.f)
      return None

  def __set__(self, obj, val):
      raise AttributeError("Illegal operation: cannot set read-only attribute")

  def __delete__(self, obj):
      raise AttributeError("Illegal operation: cannot delete attribute")


def rxrange(start, end=None, step=1):
  if end is None:
    end = start
    start = 0
  return range(end-1, start-1, -step)

def rrange(start, end=None, step=1):
  if end is None:
    end = start
    start = 0
  return list(range(end-1, start-1, -step))


def average(lst, ignoreNone=False):
  total=0.0
  n=0
  for x in lst:
    if not (ignoreNone and None==x):
      total += x
      n+=1
  return float(total)/n


def AND(a, b):
  return a and b

def OR(a, b):
  return a or b

def NAND(a, b):
  return not (a and b)

def NOR(a, b):
  return not (a or b)

def XOR(a, b):
  return (a or b) and not (a and b)


# A generalised join function, that can join lists of non-string objects
# by Sebastian Kelm
#
def join(delim, lst):
    delim = str(delim)
    l = len(lst)
    if l < 1:
        return ""

    a = ""
    a += str(lst[0])
    i=1
    while i<l:
      a += delim
      a += str(lst[i])
      i+=1
    return a


def findAll(sequence, value):
    result=[]
    for i,v in enumerate(sequence):
      if v==value:
        result.append(i)
    return result


def findAllBoolean(sequence, value):
    result=[False] * len(sequence)
    for i,v in enumerate(sequence):
      if v==value:
        result[i] = True
    return result


def tokenize2(separators, seq):
  """tokenize2(separators, seq) : Transforms any type of sequence into a list of words and a list of starting indeces

  seq : the sequence (any sequence type, e.g. a list, tuple, string, ...) [will not be modified]
  separators : a sequence of values to be used as separators between words. adjoining separators will be merged.

  Returns: words, starting_indeces
           where
              len(starting_indeces) = len(words) + 1
              starting_indeces[-1] = len(seq)

  If seq is a string, words are also returned as strings. Otherwise every word is a list. Starting indeces are integers >= 0.
  """
  words, gaps = tokenize(separators, seq)

  starting_indices = []
  n=0
  for w, g in zip(words, gaps[:-1]):
    n += g
    starting_indices.append(n)
    n += len(w)
  assert n+gaps[-1] == len(seq)
  starting_indices.append(len(seq))

  return words, starting_indices


def tokenize(separators, seq):
  """tokenize(separators, seq) : Transforms any type of sequence into a list of words and a list of gap lengths

  seq : the sequence (any sequence type, e.g. a list, tuple, string, ...) [will not be modified]
  separators : a sequence of values to be used as separators between words. adjoining separators will be merged.

  Returns: words, gaps
           where len(gaps) = len(words) + 1

  The first and last gaps are at the beginning and end of the sequence and may have length 0.

  If seq is a string, words are also returned as strings. Otherwise every word is a list. Gap lengths are integers >= 0.
  """
  words = []
  gaps  = [0]
  if len(seq)<1:
    return words, gaps

  gapIsOpen=True # current gap size
  for i,v in enumerate(seq):
    if v not in separators:
      if gapIsOpen:
        words.append([])
        gapIsOpen=False
      words[-1].append(v)
    else:
      if not gapIsOpen:
        gaps.append(0)
        gapIsOpen=True
      gaps[-1] += 1

  if not gapIsOpen:
    gaps.append(0)

  assert len(gaps) == len(words) + 1

  if isinstance(seq, str):
    for i in range(len(words)):
      words[i] = "".join(words[i])

  return words, gaps


def reduceTokens(words, gaps, wordcutoffs=0, minwordlength=1, wordValidator=None):
    """reduceTokens(words, gaps, wordcutoffs=0, minwordlength=1, wordValidator=None) : cuts off values from both sides of each token and deletes words that are then below a given length or that do not meet the condition given by the wordValidator(words, gaps, i) function. gap lengths are raised to reflect the changes. wordValidator should return True if the word is to be kept or False if the word should be deleted.

    Returns void. The input lists will be modified instead.
    """
    oldlength = sum(gaps)
    i=0
    while i < len(words):
      w = words[i]
      oldlength += len(w)
      if len(w) < (2*wordcutoffs)+minwordlength or (None != wordValidator and not wordValidator(words, gaps, i)):
        # Remove words that are too short
        gaps[i+1] += gaps[i] + len(w)
        del words[i]
        del gaps[i]
        continue
      else:
        # Modify words that are long enough
        words[i] = words[i][wordcutoffs:len(w)-wordcutoffs]
        gaps[i] += wordcutoffs
        gaps[i+1] += wordcutoffs
      i+=1

    newlength = sum(gaps)
    for w in words:
      newlength += len(w)

    assert newlength == oldlength


def deTokenize(separator, words, gaps):
    """deTokenize(separator, words, gaps) : reverse the work of the function tokenize(.). Returns a single sequence, where gaps are filled using separator.

    separator : a single value to be used to fill gaps
    words, gaps : lists of words and gap lengths, as returned by the function tokenize(.)
    """
    assert len(words) + 1 == len(gaps)
    seq=[]

    for i,w in enumerate(words):
      for j in range(gaps[i]):
        seq.append(separator)
      seq.extend(w)
    for j in range(gaps[-1]):
      seq.append(separator)

    if len(words) and isinstance(words[0], str):
      seq = "".join(seq)
    return seq


def binarySearch(lst, value, low=0, high=None):
    if high == None:
      high = len(lst)

    mid = bisect_left(lst, value, low, high)

    if mid < len(lst) and lst[mid] == value:
      return mid

    return -1 - mid


def splitpath(somepath):
    # e.g.  /path/to/file.txt

    path, basename = os.path.split(somepath)
    base, ext = os.path.splitext(basename)

    # e.g. "/path/to/", "file", ".txt"
    return (path, base, ext)


_COMPOUND_EXTENSIONS = (".pdb.gz", ".pdb1.gz", ".ent.gz")
def basename_noext(fname):
  "Get a file's basename with the extension removed, including a number of predefined compound extensions"
  fname = os.path.basename(fname)
  for ext in _COMPOUND_EXTENSIONS:
    if fname.endswith(ext):
      return fname[:-len(ext)]
  return os.path.splitext(fname)[0]


class ParsingError(RuntimeError):
    pass

class IllegalStateError(RuntimeError):
    pass
IllegalState = IllegalStateError

class DependencyError(RuntimeError):
    pass

class ExecutableDependencyError(DependencyError):
  pass

class DatabaseDependencyError(DependencyError):
  pass

class ArgumentError(ValueError):
    pass

class NotFoundError(IndexError):
    pass


def read_file(fname):
  with open(fname, "rb") as f:
    txt = f.read()
  return txt

def write_file(fname, txt):
  with open(fname, "wb") as f:
    f.write(txt)

def read_table(fname, sep="\t", types=None, comment="", line_processor=None, header=False):
  if types:
    if not isinstance(types, dict):
      t = {}
      for key, value in enumerate(types):
        t[key] = value
      types = t
    else:
      types = dict(types)

  if "\n" in fname:
    txt = fname
  else:
    txt = read_file(fname)
  data = []
  for line in txt.splitlines():
    if comment and line.startswith(comment):
      continue
    if not line:
      continue
    if header and not data:
      fields = line.split(sep)
      # Enable us to provide a typedict using column headers as keys
      for i, key in enumerate(fields):
        if key in types:
          types[i] = types[key]
          del types[key]
    elif line_processor is not None:
      fields = line_processor(line)
      if not fields:
        continue
    else:
      fields = line.split(sep)
      if types:
        for key in types:
          try:
            fields[key] = types[key](fields[key])
          except:
            print(line)
            raise
    data.append(fields)
  return data


def _write_table(fout, table, sep="\t", eol="\n", line_processor=None, header=None):
  if line_processor is None:
    line_processor = lambda row: [str(x) for x in row]

  if header is not None:
    fout.write(sep.join(line_processor(header)))
    fout.write(eol)

  for row in table:
    fout.write(sep.join(line_processor(row)))
    fout.write(eol)

def write_table(fout, table, **kwargs):
  if isinstance(fout, str):
    with open(fout, "wb") as fout:
      _write_table(fout, table, **kwargs)
  else:
    _write_table(fout, table, **kwargs)


def _write_table_from_dict(fout, dictionary, rows=None, cols=None, default=None, rowfilter=None, converter=str, header_converter=str, write_rowheader=True, write_colheader=True):
  if rows is None:
    if isinstance(dictionary, list) or isinstance(dictionary, tuple):
      rows = list(range(len(dictionary)))
    else:
      rows = sorted(dictionary)
  if cols is None:
    cols = sorted(dictionary[rows[0]])

  if write_colheader:
    fout.write("#")
    for colkey in cols:
      fout.write("\t"+header_converter(colkey))
    fout.write("\n")

  for rowkey in rows:
    row = dictionary[rowkey]
    if rowfilter is not None and not rowfilter(row):
      continue

    line = ""
    if write_rowheader:
      line = header_converter(rowkey)
    for colkey in cols:
      try:
        value = row[colkey]
      except KeyError:
        value = default
      line += "\t"+converter(value)
    if not write_rowheader:
      line = line.lstrip("\t")
    fout.write(line+"\n")


def write_table_from_dict(fout, dictionary, **kwargs):
  if isinstance(fout, str):
    with open(fout, "wb") as fout:
      _write_table_from_dict(fout, dictionary, **kwargs)
  else:
    _write_table_from_dict(fout, dictionary, **kwargs)


def read_table_to_dict(fname, typedict=None, row_processor=None, default_type=None, **kwargs):
  """Read a formatted text file into an OrderedDictionary of dictionaries.

  typedict     is a dictionary of types (or converter functions that take one argument and return one argument).
  default_type is the type / converter used for any column not handled by typedict.
  """
  if isinstance(fname, str):
    data = read_table(fname, **kwargs)
  else:
    data = fname
  colkeys = data[0]
  datadict = collections.OrderedDict()
  for i in range(1, len(data)):
    row = data[i]
    if row_processor is not None:
      row = row_processor(row)
      if not row:
        continue
    rowkey = row[0]
    valdict = {}
    for j in range(1, len(colkeys)):
      key = colkeys[j]
      x = row[j]
      if typedict is not None and key in typedict:
        try:
          x = typedict[key](x)
        except:
          print("col key:", key, file=sys.stderr)
          print("field value:", x, file=sys.stderr)
          print("type / conversion function:", typedict[key], file=sys.stderr)
          raise
      elif default_type is not None:
        x = default_type(x)
      valdict[key] = x
    datadict[rowkey] = valdict
  return datadict


def which(fname):
  "Equivalent to the 'which' linux shell command"
  path = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), fname)

  if not path or not os.path.exists(path):
    p = subprocess.Popen(shlex.split("which %s" % (fname)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    path, errors = p.communicate()
    path = path.strip()
    #if errors.strip():
    #  sys.stderr.write(errors)
    if not path or not os.path.exists(path):
      return None

    path = os.path.abspath(path)

  return path


def filter_string(inputstring, allow=(), deny=()):
  allowed = ""
  for cat in allow:
    allowed += getattr(string, cat)
  denied = ""
  for cat in deny:
    denied += getattr(string, cat)
  return "".join([x for x in inputstring if x in allowed and x not in denied])


class Data(object):
  def __str__(self):
    output = ""
    for field in dir(self):
      if not field.startswith("_"):
        output += " %s=%s," % (field, str(getattr(self, field)))
    return "<Data:"+output[:-1]+">"

  def __repr__(self):
    output = ""
    for field in dir(self):
      if not field.startswith("_"):
        output += " %s=%s," % (field, repr(getattr(self, field)))
    return "<Data:"+output[:-1]+">"


def get_python_library_root():
    "Get the directory containing the 'prosci' python library"
    return os.path.dirname(os.path.abspath(os.path.dirname(__file__)))


def file_not_empty(fname):
  if not os.path.exists(fname):
    return False
  with open(fname) as f:
    for line in f:
      if line:
        return True
  return False


def file_is_empty(fname):
  return not file_not_empty(fname)


'''
def parallelise(cpus, commands, timeout=999999):
  "Utility function to run a bunch of function calls in parallel, where 'commands' is a list of (function, arguments) tuples"
  if not commands:
    return

  from multiprocessing import cpu_count, Pool

  if cpus == 1:
    for func, args in commands:
      e = func(*args)
      if e is not None and isinstance(e, Exception):
        raise e
    return

  print "Running commands in parallel:", commands

  results = []
  errors = []
  pool = Pool(cpus)
  try:
    # Run stuff...
    for func, args in commands:
      results.append(pool.apply_async(func, args))

    pool.close()

    # Wait for results
    for r in results:
      exception = r.get(timeout=timeout)
      if exception is not None:
        try:
          print >>sys.stderr, exception.traceback_string
        except AttributeError:
          pass
        errors.append(exception)

    if errors:
      print >>sys.stderr, len(errors), "exception(s):"
      for exception in errors:
        try:
          print >>sys.stderr, exception.traceback_string
        except AttributeError:
          print >>sys.stderr, type(exception), ":", exception

  finally:
    pool.terminate()
'''


def _run(func, args):
  import traceback
  try:
    return func(*args)
  except Exception as e:
    e.traceback_string = traceback.format_exc()
    return e


def parallelise(cpus, commands, timeout=999999, verbose=False, raise_exception=False, print_exception=True):
  """
  Parallelise functions that accept any number of sequential arguments. The return values are gathered and returned in a list of the same length as the input.

  If raise_exception=True the return values are checked for their type and the first that is an instance of Exception will be raised.
  If print_exception=True, exceptions are printed to stderr, independently of raise_exception.

  commands is a list in the format [(function, sequential_arguments), (function, sequential_arguments), ...]
  """
  return_values = []
  errors = []

  if not commands:
    return return_values

  if cpus <= 0:
    cpus = cpu_count()
  cpus = min(cpus, len(commands), cpu_count())

  if cpus == 1:
    # Run stuff sequentially and check if there was an error
    for func, args in commands:
      if not isinstance(args, list) or isinstance(args, tuple):
        raise ValueError("parallelise() expects a list or tuple of arguments for each function it is running, not a %s"%(str(type(args))))
      value = func(*args)
      if (print_exception or raise_exception) and isinstance(value, Exception):
        if raise_exception:
          raise value
        errors.append(value)
        return_values.append(None)
      else:
        return_values.append(value)
  else:
    if verbose:
      print("Running %d commands in parallel..." % len(commands))

    pool = Pool(cpus)
    try:
      processes = []
      # Run stuff in parallel
      for func, args in commands:
        processes.append(pool.apply_async(_run, (func, args)))
      pool.close()

      # Wait for processes to finish and check if there was an error
      for P in processes:
        value = P.get(timeout=timeout)
        if (print_exception or raise_exception) and isinstance(value, Exception):
          if raise_exception:
            raise value
          errors.append(value)
          return_values.append(None)
        else:
          return_values.append(value)
    finally:
      pool.terminate()

  # Print any errors if the user wants us to
  if errors:
    print(len(errors), "exception(s):", file=sys.stderr)
    for exception in errors:
      try:
        print(exception.traceback_string, file=sys.stderr)
      except AttributeError:
        print(type(exception), ":", exception, file=sys.stderr)

  return return_values


def run_systemcall(command, cwd=None):
  "Run a system call via the subprocess module"
  import traceback
  try:
    subprocess.call(command, shell=True, cwd=cwd)
  except Exception as e:
    e.traceback_string = traceback.format_exc()
    return e
  return None


def read_excel(filename, sheet=0):
	"Read a single sheet in an Excel spreadsheet file"
	import xlrd
	wb = xlrd.open_workbook(filename)
	sh = wb.sheet_by_index(sheet)
	output = []
	for i in range(sh.nrows):
		output.append(sh.row_values(i))
	return output


def get_url(url, fname=None):
  import requests as rq
  r = rq.get(url)
  if r.status_code != 200:
    return None
  if fname is not None:
    with open(fname, "w") as f:
      f.write(r.content)
  return r.content


def split_num_boundary(a):
    "Split a string at all digit-nondigit boundaries"
    fields = re.split(r"(\d\D|\D\d)", a)
    for i in range(1, len(fields), 2):
        left, right = fields[i]
        fields[i-1] += left
        fields[i+1] = right + fields[i+1]
        fields[i] = ''
    fields = [x for x in fields if x]
    return fields


def split_integers(a):
    "Split a string at all digit-nondigit boundaries and convert digits to integers"
    a = split_num_boundary(a)
    try:
      return [int(a[i]) if i%2 else a[i] for i in range(len(a))]
    except ValueError:
      return [int(a[i]) if not i%2 else a[i] for i in range(len(a))]


def write_pickle(fname, obj):
  "Write an object to a pickle file"
  import pickle as pickle
  with open(fname, "wb") as f:
    pickle.dump(obj, f)


def read_pickle(fname, default=None):
  "Read an object from a pickle file"
  import pickle as pickle
  try:
    with open(fname, "rb") as f:
      return pickle.load(f)
  except OSError:
    if default is not None:
      return default
    raise


#### NOSE TESTS


def test_split_num_boundary():
    a = "one1234two5678three"
    fields = split_num_boundary(a)
    assert(fields == ['one', '1234', 'two', '5678', 'three'])

