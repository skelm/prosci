# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys

paths = []
#paths.append("/path/to/executables")

pythonpaths = []
#pythonpaths.append("/path/to/python/libraries")

if paths:
  os.environ["PATH"] = ":".join(paths)+":"+os.environ["PATH"]

if pythonpaths:
  if "PYTHONPATH" not in os.environ:
    os.environ["PYTHONPATH"] = ":".join(pythonpaths)
  else:
    os.environ["PYTHONPATH"] = os.environ["PYTHONPATH"] + ":" + ":".join(pythonpaths)
  sys.path.extend(pythonpaths)
