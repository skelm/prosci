#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import subprocess
from prosci.util.protein import Protein


VALID_CHAIN_CODES = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']


def _parse_proteins(p1, p2, atom_types=None):
  "Prepare two protein objects for feeding to pinterface"

  if not isinstance(p1, Protein):
    p1 = Protein(p1)
  if not isinstance(p2, Protein):
    p2 = Protein(p2)

  if atom_types is not None:
    if isinstance(atom_types, str):
      atom_types = (atom_types, )
    atoms1 = []
    for a in p1.iter_atoms():
      if a.atom in atom_types:
        atoms1.append(a)
    atoms2 = []
    for a in p2.iter_atoms():
      if a.atom in atom_types:
        atoms2.append(a)
    p1 = Protein(atoms1)
    p2 = Protein(atoms2)

  p1_chaincodes = sorted(set([x.chain for x in p1] + [x.chain for x in p1.ligands]))
  p2_chaincodes = sorted(set([x.chain for x in p2] + [x.chain for x in p2.ligands]))

  # Find new names for chains with clashing names
  chain_mapping = {}
  for c2 in p2_chaincodes:
    c2b = c2
    while c2b in p1_chaincodes or c2b in list(chain_mapping.values()):
      c2b = VALID_CHAIN_CODES[(VALID_CHAIN_CODES.index(c2b) + 1) % len(VALID_CHAIN_CODES)]
    chain_mapping[c2] = c2b

  # Rename clashing chain names in p2
  if chain_mapping:
    p2 = Protein(str(p2), code=p2.code)
    for chain in p2:
      if chain.chain in chain_mapping:
        chain.chain = chain_mapping[chain.chain]

  p2_chaincodes_changed = sorted(set([x.chain for x in p2] + [x.chain for x in p2.ligands]))
  chain_mapping_reversed = dict([(v, k) for k, v in list(chain_mapping.items())])

  return p1, p2, p1_chaincodes, p2_chaincodes_changed, chain_mapping_reversed


def get_min_contact_distance(p1, p2, atom_types=None, max_dist=20.0):
  "Find the pair of residues in the two given domains that are closest"

  p1, p2, p1_chaincodes, p2_chaincodes_changed, chain_mapping_reversed = _parse_proteins(p1, p2, atom_types)

  # Run pinterface
  outtxt = subprocess.Popen(["pinterface", "-v", "-d", str(max_dist), "-"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True).communicate(str(p1)+str(p2))[0]

  # Parse the output of pinterface
  minpair = []
  for line in outtxt.splitlines():
    if line.startswith("Mindist: "):
      line = line[len("Mindist: "):]
      fields = eval(line)
      atm1, atm2, d, d_minus_vdw = fields
      if atm1[0] in p2_chaincodes_changed and atm2[0] in p1_chaincodes:
        # reverse order interface between p1 and p2
        tmp = atm1
        atm1 = atm2
        atm2 = tmp
        del tmp
      if atm1[0] in p1_chaincodes and atm2[0] in p2_chaincodes_changed:
        # interface between p1 and p2
        if not minpair or minpair[-1] > d:
          minpair = [atm1, atm2, d]

  if not minpair:
    print("P1:", file=sys.stderr)
    print(str(p1), file=sys.stderr)
    print("P2:", file=sys.stderr)
    print(str(p2), file=sys.stderr)
    print("output:", outtxt, file=sys.stderr)
    print("max_dist", max_dist, file=sys.stderr)
    print("Minpair:", minpair, file=sys.stderr)
    raise ValueError("No interface between given domains")

  atm1, atm2, d = minpair

  if atm2[0] in chain_mapping_reversed:
    atm2 = list(atm2)
    atm2[0] = chain_mapping_reversed[atm2[0]]
    atm2 = tuple(atm2)

  return atm1, atm2, d


def get_contact_residues(p1, p2, atom_types=None, max_dist=None):
  "Find the residues at the interface between the two given Protein structures"

  p1, p2, p1_chaincodes, p2_chaincodes_changed, chain_mapping_reversed = _parse_proteins(p1, p2, atom_types)

  ## Run pinterface
  #
  if max_dist is None:
    outtxt = subprocess.Popen(["pinterface", "-"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True).communicate(str(p1)+str(p2))[0]
  else:
    outtxt = subprocess.Popen(["pinterface", "-d", str(max_dist), "-"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True).communicate(str(p1)+str(p2))[0]

  # Parse the output of pinterface
  #data = []
  residue_ids1 = []
  residue_ids2 = []
  for line in outtxt.splitlines():
    fields = eval(line)
    chain1, ires1, inscode1, chain2, count = fields
    if chain1 in p1_chaincodes and chain2 in p2_chaincodes_changed:
      residue_ids1.append((chain1, ires1, inscode1))
    elif chain1 in p2_chaincodes_changed and chain2 in p1_chaincodes:
      residue_ids2.append((chain_mapping_reversed[chain1], ires1, inscode1))

  return residue_ids1, residue_ids2


