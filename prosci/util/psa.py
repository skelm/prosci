#!/usr/bin/env python
from __future__ import print_function
import os
import subprocess
import tempfile
import shutil

from prosci.common import write_file
from prosci.util.protein import Protein, ResidueList, Pdb


def calculate_surface_accessibility_boolean(pdbfile, bcutoff=0.1):
  """List residues with any surface accessibile atoms.

  Requires the binary 'psa'."""

  usetemp = False
  if not isinstance(pdbfile, str):
    # We are dealing with an object. Need to write a temporary file.
    usetemp = True
    tmpdir = tempfile.mkdtemp()

  try:

    if usetemp:
      fname = os.path.join(tmpdir, "structure.atm")
      with open(fname, "w") as f:
        f.write(str(pdbfile))
      pdbfile = fname

    atmfile = os.path.splitext(pdbfile)[0]+".atm"
    solfile = os.path.splitext(pdbfile)[0]+".sol"

    if not os.path.exists(atmfile):
      structure = ResidueList(pdbfile)
      write_file(atmfile, str(structure))

    if not os.path.exists(solfile):
      if usetemp:
        out, err = subprocess.Popen(["psa", "-a", "structure.atm"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=tmpdir, universal_newlines=True).communicate()
      else:
        out, err = subprocess.Popen(["psa", "-a", os.path.basename(atmfile)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.dirname(os.path.abspath(atmfile)), universal_newlines=True).communicate()
      if not os.path.exists(solfile):
        raise Exception("PSA did not produce the expected file: %s\nPSA STDOUT:\n%s\nPSA STDERR:\n%s\n" %(solfile, out, err))

    structure = ResidueList(Pdb(solfile, nofilter=True))
    assert len(structure) > 0

  finally:
    if usetemp:
      shutil.rmtree(tmpdir)

  access = []
  for res in structure:
    isaccessible = False
    for a in res:
      if not (a.atom.startswith("H") or a.atom[0:1].isdigit()) and a.b > bcutoff:
        isaccessible = True
        break
    access.append(isaccessible)
  assert len(access) == len(structure)

  return access


def annotate_surface_accessibility(pdbfile, bcutoff=0.1):
  """Calculate surface accessibility for each atom and each residue.

  Returns a Protein object with each atom's B factor being replaced by its accessibility in square Angstroms.
  Residues will also have "access", "access_mainchain" and "access_sidechain" attributes with percentage values (as fractions of 1.0). The "accessible" property will reflect if there is a heavy atom in the residue that has more than 'bcutoff' surface exposure (default:0.1).
  Requires the binary 'psa'.
  """

  usetemp = False
  if not isinstance(pdbfile, str):
    # We are dealing with an object. Need to write a temporary file.
    usetemp = True
    tmpdir = tempfile.mkdtemp()

  try:

    if usetemp:
      fname = os.path.join(tmpdir, "structure.atm")
      with open(fname, "w") as f:
        f.write(str(pdbfile))
      pdbfile = fname

    atmfile = os.path.splitext(pdbfile)[0]+".atm"
    solfile = os.path.splitext(pdbfile)[0]+".sol"
    psafile = os.path.splitext(pdbfile)[0]+".psa"

    if not os.path.exists(atmfile):
      structure = ResidueList(pdbfile)
      write_file(atmfile, str(structure))

    if not os.path.exists(solfile) or not os.path.exists(psafile):
      if usetemp:
        out, err = subprocess.Popen(["psa", "-a", "structure.atm"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=tmpdir, universal_newlines=True).communicate()
      else:
        out, err = subprocess.Popen(["psa", "-a", os.path.basename(atmfile)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.dirname(os.path.abspath(atmfile)), universal_newlines=True).communicate()
      if not os.path.exists(solfile) or not os.path.exists(psafile):
        raise Exception("PSA did not produce the expected files: %s\nPSA STDOUT:\n%s\nPSA STDERR:\n%s\n" %(solfile+", "+psafile, out, err))

    structure = ResidueList(Pdb(solfile, nofilter=True))
    psa = parse_psa(psafile)
    assert len(structure) > 0

  finally:
    if usetemp:
      shutil.rmtree(tmpdir)

  assert len(psa) == len(structure)

  for res, access in zip(structure, psa):
    assert res.ires == access[0]
    assert res.res == access[1]
    res.access = access[2]
    res.access_sidechain = access[3]
    res.access_mainchain = access[4]
    isaccessible = False
    for a in res:
      if not (a.atom.startswith("H") or a.atom[0:1].isdigit()) and a.b > bcutoff:
        isaccessible = True
        break
    res.accessible = isaccessible

  return Protein(structure)


def get_surface_accessibility_dict(pdbfile, bcutoff=0.1):
  prot = annotate_surface_accessibility(pdbfile, bcutoff)
  output = {}
  for chain in prot:
    for res in chain:
      output[res.get_id()] = (res.access, res.access_sidechain, res.access_mainchain, res.accessible)
  return output



def parse_psa(psafile):
  "Parse PSA file and extract the sidechain percentage accessibility for each residue"
  accessibilities = []
  with open(psafile) as f:
    for line in f:
      if line.startswith('ACCESS'):
        resnum = int(line[6:11])
        restype = line[11:18].strip()
        allatom = float(line[25:30])/100.0 # all atom percentage accessibility
        sidechain = float(line[61:66])/100.0 # sidechain percentage accessibility
        mainchain = float(line[73:78])/100.0 # mainchain percentage accessibility
        accessibilities.append((resnum, restype, allatom, sidechain, mainchain))
        #print resnum, restype, access
  return accessibilities

def parse_psa_all_atom(psafile):
  "Parse PSA file and extract the all-atom percentage accessibility for each residue"
  return [(x[0], x[1], x[2]) for x in parse_psa(psafile)]

def parse_psa_sidechain(psafile):
  "Parse PSA file and extract the sidechain percentage accessibility for each residue"
  return [(x[0], x[1], x[3]) for x in parse_psa(psafile)]

def parse_psa_mainchain(psafile):
  "Parse PSA file and extract the mainchain percentage accessibility for each residue"
  return [(x[0], x[1], x[4]) for x in parse_psa(psafile)]

