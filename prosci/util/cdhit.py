#!/usr/bin/env python
from __future__ import print_function
import os
import shutil
from prosci.util.tempdir import TempDir


def cdhit(id_fraction, seqs, out_fasta=None, out_clstr=None, skip_existing=False):
  "Run CD-HIT to cluster and filter a set of sequences"
  if id_fraction < 0.4:
    raise ValueError("Cannot have identity cut-offs below 0.4")
  if skip_existing:
    if os.path.exists(out_fasta) and os.path.exists(out_clstr):
      outseqs = d.read_file(out_seqfile).splitlines()[1::2]
      clusters = d.read_file(out_seqfile+".clstr")
      return outseqs, clusters
  
  in_seqfile = "input.fasta"
  out_seqfile = "output.fasta"
  with TempDir(keep=False) as d:
    #print "TempDir.path:", d.path
    if isinstance(seqs, str):
      if len(seqs.splitlines()) == 1:
        shutil.copy(seqs, d.get_path(in_seqfile))
      else:
        with d.open(in_seqfile, "w") as f:
          f.write(seqs)
    else:
      with d.open(in_seqfile, "w") as f:
        for s in seqs:
          f.write(">%s\n%s\n"%(s, s))
    with open(os.devnull, "wb") as DEVNULL:
      p = d.run_systemcall("cd-hit -T 0 -i '%s' -o '%s' -c %.2f -n 2 -l 3 -g 0 -sc 1 -sf 1"%(in_seqfile, out_seqfile, id_fraction), stdout=DEVNULL)
    #print "Return code:", p.returncode
    if p.returncode != 0:
      raise RuntimeError("CD-HIT crashed")
    outseqs = d.read_file(out_seqfile).splitlines()[1::2]
    clusters = d.read_file(out_seqfile+".clstr")
    if out_fasta:
      d.move_file(out_seqfile, out_fasta)
    if out_clstr:
      d.move_file(out_seqfile+".clstr", out_clstr)
  return outseqs, clusters


def cluster_loop_seqs(seqs, mutations_within_cluster, out_fasta=None, out_clstr=None):
  "Cluster a list of loop sequences, such that each cluster contains sequences with at most a given number of mutations from each other"
  length = len(seqs[0])
  fraction = (length - mutations_within_cluster - 0.5)/float(length)
  
  if not out_fasta:
    out_fasta="length_%02d_filtered.fasta"%length
  if not out_clstr:
    out_clstr="length_%02d_filtered.clstr"%length
  
  return cdhit(fraction, seqs, out_fasta=out_fasta, out_clstr=out_clstr)



### NOSE TESTS


def test_cluster_loop_seqs():
  seqs = """
  VLGG
  VRGG
  VRTT
  YCAR
  YCTS
  YCTT
  """.split()
  
  filtered_seqs, clusters = cluster_loop_seqs(seqs, 1)
  
  assert filtered_seqs == ['VLGG', 'VRTT', 'YCAR', 'YCTS']
  assert clusters == '>Cluster 0\n0\t4aa, >YCTS... *\n1\t4aa, >YCTT... at 75.00%\n>Cluster 1\n0\t4aa, >VLGG... *\n1\t4aa, >VRGG... at 75.00%\n>Cluster 2\n0\t4aa, >YCAR... *\n>Cluster 3\n0\t4aa, >VRTT... *\n'

  

if __name__ == "__main__":
  test_cluster_loop_seqs()

