#!/usr/bin/env python
from __future__ import print_function
from prosci.common import read_pickle, write_pickle, parallelise

class PickleJar(dict):
  "A dictionary buffered in a pickle file."
  
  def __init__(self, fname, keys=None, filler_func=None, cpus=0):
    self.fname = fname
    self.load()
    if keys is not None:
      self.fill(keys, filler_func, cpus)

  def __enter__(self):
    return self

  def __exit__(self, *args):
    self.save()
  
  #def __del__(self):
  #  self.save()
  
  def __repr__(self):
    return "PickleJar(%s)" % (repr(self.fname))
  
  def load(self):
    try:
      self.update(read_pickle(self.fname, {}))
    except IOError:
      pass
  
  def save(self):
    write_pickle(self.fname, self)
    
  def fill(self, keys, filler_func, cpus=0):
    "Fill the PickleJar by generating a bunch of values in parallel, if they are not yet in the jar."
    keys = [k for k in keys if k not in self]
    if keys:
      results = parallelise(cpus, [(filler_func, [k]) for k in keys])
      for k, v in zip(keys, results):
        self[k] = v
      self.save()

