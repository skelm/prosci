#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Protein class represents a protein, as defined by a PDB file.
#
# Author: Sebastian Kelm
# Created: 19/04/2012
#
from __future__ import print_function
import numpy

from prosci.common import write_file
from prosci.util.pdb import Pdb, Atom, residueLetter, residueCode
from prosci.util.residue import BaseResidueList, ResidueList, BaseResidue, Residue, SequenceMappingError, AtomTypeNotFoundError, ALTLOCS
from prosci.util.gaps import isGap
from prosci.util.ali import Ali

class Protein(list):
  "A list of ResidueList objects, each of which represents one PDB chain."

  def __init__(self, pdb, code=None, ligands=None, cryst1="", filename=""):
    "Takes a Pdb object or a list of ResidueLists as the first argument. Otherwise passes the argument to Pdb() first, which can deal with filenames. Second argument is an optional short decription (usually a PDB code)."
    self.code = ""
    self.ligands = ()
    self.cryst1 = cryst1
    self.filename = filename

    if not pdb:
      pass
    elif not isinstance(pdb, Pdb):
      if isinstance(pdb, Protein) or isinstance(pdb[0], BaseResidueList):
        for reslist in pdb:
          self.append(ResidueList(reslist))
      elif isinstance(pdb, BaseResidueList):
        if len(pdb) > 0:
          self.extend(ResidueList(pdb).split_chains())
      else:
        pdb = Pdb(pdb)
      try:
        self.code = pdb.code
      except AttributeError:
        self.code = pdb[0].code
      try:
        if not self.cryst1:
          self.cryst1 = pdb.cryst1
      except AttributeError:
        pass
      try:
        if not self.filename:
          self.filename = pdb.filename
      except AttributeError:
        pass

    if isinstance(pdb, Pdb):
      if len(pdb) > 0:
        self.extend(ResidueList(pdb).split_chains())
      self.code = pdb.code

    if code is not None:
      self.code = code

    try:
      self.add_ligands(pdb.ligands)
    except AttributeError:
      pass

    self.add_ligands(ligands)

  def write(self, fname):
    write_file(fname, str(self))

  def delete_residues_before_1(self):
    "Get rid of residues before number 1 (typically HIS tags etc.)"
    for chain in self:
      chain.delete_residues_before_1()
  
  def delete_residues_without_CA(self):
    "Get rid of residues that don't have a CA atom"
    for chain in self:
      chain.delete_residues_without_CA()
  
  def __repr__(self):
    return "Protein(%s, %s, %s, cryst1=%s, filename=%s)"%(list.__repr__(self), repr(self.code), repr(self.ligands), repr(self.cryst1), repr(self.filename))

  def __str__(self):
    out=self.cryst1
    for r in self:
      if r:
        out += str(r)
    for r in self.ligands:
      if r:
        out += str(r)
    return out

  def __getslice__(self, start=None, end=None):
    return Protein(list.__getslice__(self, start, end), self.code, cryst1=self.cryst1, filename=self.filename)
    # Slicing a Protein means taking only certain chains. I guess we don't need to include ligands here...


  def add_ligands(self, ligands, distance=None):
    if not ligands:
      return
    if not isinstance(ligands, Protein):
      #print repr(ligands)
      #print Pdb(ligands, allowLigands=True)
      ligands = Protein(ResidueList(Pdb(ligands, allowLigands=True)).split_chains())
    if distance and ligands:
      prot_coords = []
      for chain in self:
        for res in chain:
          if res.CA is not None:
            prot_coords.append(res.CA.xyz)
      selected_ligs = ResidueList([])
      for ligchain in ligands:
        for lig in ligchain:
          centroid = get_residue_centroid(lig)
          for xyz in prot_coords:
            if numpy.linalg.norm(xyz-centroid) < distance:
              selected_ligs.append(lig)
              break
      if selected_ligs:
        ligands = Protein(selected_ligs)
    if self.ligands:
      self.ligands.extend(ligands)
    else:
      self.ligands = ligands


  def get_chain(self, chaincode):
    "Get a particular chain by its chain code"
    for chain in self:
      if chain.chain == chaincode:
        return chain
    for chain in self.ligands:
      if chain.chain == chaincode:
        return chain
    raise KeyError("Chain %s not found in Protein object with code '%s'" % (chaincode, self.code))


  def get_seq(self):
    "Returns a FASTA string representation of the sequences."
    seq = ""
    for chain in self:
      if chain is not None:
        seq += chain.get_seq()+"/"
    return seq[:-1]

  def to_ali(self):
    "Returns an Ali object with each chain's sequence, and entries titled self.code+chain.chain"
    seq = ""
    for chain in self:
      if chain is not None:
        seq += ">%s\nstructure\n%s*\n" % (self.code+chain.chain, chain.get_seq())
    return Ali(seq)

  def get_coords(self, atomfilter=lambda atom: True, ligands=False):
    "Get a numpy array of coordinates"
    coords=[]
    for chain in self:
      for r in chain:
        if r is not None:
          for a in r:
            if atomfilter(a):
              coords.append(a.xyz)
    if ligands:
      for chain in self.ligands:
        for r in chain:
          if r is not None:
            for a in r:
              if atomfilter(a):
                coords.append(a.xyz)
    return numpy.array(coords)


  def to_pdb(self, atomfilter=lambda atom: True):
    "Returns a Pdb object containing the Atom objects within this object"
    p = Pdb(self.code, [], cryst1=self.cryst1, filename=self.filename)
    for chain in self:
      for res in chain:
        for atm in res:
          if atomfilter(atm):
            p.data.append(atm)
    for chain in self.ligands:
      for res in chain:
        for atm in res:
          if atomfilter(atm):
            p.ligands.data.append(atm)
    return p


  def iter_atoms(self, ligands=True):
    for chain in self:
      for r in chain:
        for a in r:
          yield a
    if ligands:
      for chain in self.ligands:
        for r in chain:
          for a in r:
            yield a


  def to_residuelist(self, ligands=False):
    "Returns a single ResidueList object"
    rl = ResidueList([], self.code)
    for chain in self:
      rl.extend(chain)
    if ligands:
      for chain in self.ligands:
        rl.extend(chain)
    return rl

  def renumber(self, start=1):
    "Renumber residues in each chain from 1 upwards"
    for chain in self:
      for i, r in enumerate(chain):
        r.ires = i+start
        r.inscode = ""
    for chain in self.ligands:
      for i, r in enumerate(chain):
        r.ires = i+start
        r.inscode = ""
    self.renumber_atoms()

  def renumber_atoms(self):
    "Renumbers all atoms, starting from 1 upwards"
    i = 1
    for chain in self:
      for r in chain:
        for a in r:
          a.iatom = i
          i += 1
    for chain in self.ligands:
      for r in chain:
        for a in r:
          a.iatom = i
          i += 1


  def remove_residues(self, func, ligands=False):
    i=0
    while i < len(self):
      self[i].remove_residues(func)
      if not self[i]:
        del self[i]
      else:
        i += 1

    if ligands:
      i=0
      while i < len(self.ligands):
        self.ligands[i].remove_residues(func)
        if not self.ligands[i]:
          del self.ligands[i]
        else:
          i += 1


  def iter_backbone(self):
    "Iterate over all backbone atoms that aren't None"
    for chain in self:
      for r in chain:
        for a in r.iter_backbone():
          yield a


  def find_residue(self, ires, inscode="", chain="", reverse=False):
    instring = None
    if isinstance(ires, BaseResidue) or isinstance(ires, Atom):
      inscode = ires.inscode
      chain = ires.chain
      ires = ires.ires
    elif isinstance(ires, str):
      instring = ires
      if ires[:1].isalpha():
        chain = ires[:1]
        ires = ires[1:]
      if ires[-1:].isalpha():
        inscode = ires[-1:]
        ires = ires[:-1]
      ires = int(ires)
    elif not isinstance(ires, int) and len(ires) in (2, 3) and not inscode and not chain:
      if len(ires) == 2:
        chain, ires = ires
      else:
        chain, ires, inscode = ires

    result = (-1, -1)
    for ichain, chainresidues in enumerate(self):
      if chainresidues.chain == chain:
        result = (ichain, chainresidues.find_residue(ires, inscode, chain, reverse=reverse))
        break

    if result == (-1, -1) and instring is not None:
      result = self.find_residue(instring[1:], chain=instring[:1], reverse=reverse)

    return result
  
  
  def get_residue(self, res_id, reverse=False):
    i, j = self.find_residue(res_id, reverse=reverse)
    assert i>=0 and j>=0, "Could not find residue %s"%repr(res_id)
    return self[i][j]
  

  def map_to_seq(self, seq, renumber=False, start_number=1):
    output = []
    startoffset = 0
    for chain in self:
      #print "Mapping chain", chain.chain, "with sequence:", chain.get_seq(), "to sequence:", seq[startoffset:], startoffset, renumber, start_number
      subseq = seq[startoffset:]
      try:
        # Skip ends of previously matched chains whose termini haven't been
        # modelled and therefore not mapped.
        #
        i_chainbreak = subseq.index("/")
        if i_chainbreak < len(chain):
          startoffset += i_chainbreak + 1
          subseq = seq[startoffset:]
      except ValueError:
        pass
      indices = chain.map_to_seq(subseq, renumber, start_number)
      if startoffset:
        for i, x in enumerate(indices):
          indices[i] = x + startoffset
      startoffset = indices[-1]+1
      for i in range(startoffset, len(seq)):
        if seq[i] == "/":
          startoffset += 1
        else:
          break
      output.append(indices)
    return output


def get_residue_centroid(res):
  "Calculate the centroid of all atom positions in the given residue"
  cen = numpy.array([0.0, 0.0, 0.0])
  N = 0
  for a in res:
    if a is not None:
      cen += a.xyz
      N += 1
  return cen / N


## Compatibility

class AltResidueList(ResidueList):
  pass

class AltResidue(Residue):
  pass

