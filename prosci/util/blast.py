#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
import re
import shutil
import tempfile
import subprocess
import multiprocessing
from operator import itemgetter

from prosci.common import IllegalStateError
from prosci.util.ali import Ali

re_error = re.compile("error", re.IGNORECASE)


def make_blast_database(infile, outname):
  "Make a protein BLAST database"

  outname = os.path.abspath(outname)
  name = os.path.basename(outname)
  workdir = os.path.dirname(outname)

  if os.path.exists(outname+".pin") or os.path.exists(outname+".00.pin"):
    return

  if not os.path.exists(workdir):
    os.mkdir(outdir)

  if not os.path.exists(outname):
    if not isinstance(infile, str) or infile.count("\n")>1:
      if isinstance(infile, Ali):
        infile = infile.toFastaString()
      with open(outname, "w") as f:
        f.write(str(infile))
    else:
      shutil.copy(infile, outname)


  command = "makeblastdb -in %s -dbtype prot -parse_seqids -hash_index" % (name)
  p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=workdir, universal_newlines=True)
  result, errors = p.communicate()
  for l in errors.splitlines():
    if re_error.search(l):
      raise IllegalStateError("makeblastdb crashed: '%s'" % (l))


def run_psiblast(dbpath, inputid, inputseq, evalue=0.001, hvalue=0.00001, iterations=5, cpus=1, fragments=True, min_identity=15.0):
  "Run PSIBLAST and return the top hits"
  # Write a temporary query sequence file
  cpus = min(cpus, multiprocessing.cpu_count())
  assert cpus >= 1
  fd, tmpfname = tempfile.mkstemp()
  try:
    f = os.fdopen(fd, 'w')
    try:
      f.write(">%s\n%s\n"%(inputid, inputseq))
    finally:
      f.close()

    command = "psiblast -query '%s' -db '%s' -num_iterations %d -evalue %f -inclusion_ethresh %f -outfmt 6 -num_threads %d" % (tmpfname, dbpath, iterations, evalue,    hvalue, cpus)
    #print command
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    result, errors = p.communicate()
  finally:
    os.remove(tmpfname)

  for l in errors.splitlines():
    if re_error.search(l):
      raise IllegalStateError("BLAST crashed: '%s'" % (l))

  resultlist = []
  for l in result.splitlines():
    fields = l.strip().split()
    try:
      accession = fields[1]
      evalue = float(fields[10])
      identity = float(fields[2])
      start = int(fields[8])
      end = int(fields[9])
    except IndexError:
      continue
    except ValueError:
      continue
    if identity >= min_identity:
      resultlist.append([accession, evalue, identity, start, end])
  resultlist.sort(key=itemgetter(1), reverse=True)

  # Make sure only to return unique matches, with the lowest E-value per match
  resultdict = {}
  for accession, evalue, identity, start, end in resultlist:
    if fragments:
      key = (accession, start, end)
    else:
      key = accession
    resultdict[key] = (accession, evalue, identity, start, end)

  return sorted(list(resultdict.values()), key=itemgetter(1))


def pick_unique_blast_hits(hits):
  "Make sure only to return unique matches, with the lowest E-value per match"

  resultdict = {}
  for accession, evalue, identity, start, end in sorted(hits, key=itemgetter(1), reverse=True):
    resultdict[accession] = (accession, evalue, identity, start, end)

  return sorted(list(resultdict.values()), key=itemgetter(1))


def retrieve_blast_sequences(blastdb, hits, outfname=None, fragments=False, shortnames=False):
  "Retrieve the sequence for each hit in the given table of hits"

  if not fragments:
    fd, tmpfname = tempfile.mkstemp()
    try:
      f = os.fdopen(fd, 'w')
      try:
        f.write("\n".join([x[0] for x in hits])+"\n")
      finally:
        f.close()

      command = """blastdbcmd -db "%s" -entry_batch "%s" -outfmt '%%f' """%(blastdb, tmpfname)
      p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
      result, errors = p.communicate()
    finally:
      os.remove(tmpfname)

  else:
    hit_seqs = []
    for accession, evalue, identity, start, end in hits:
      p = subprocess.Popen("""blastdbcmd -db "%s" -entry %s -range %d-%d"""%(blastdb, accession, start, end), shell=True, stdout=subprocess.PIPE, universal_newlines=True)
      out = p.communicate()[0]
      hit_seqs.append(out)
    result = "".join(hit_seqs)

  if shortnames:
    result = truncate_names(result)

  if outfname:
    with open(outfname, "w") as f:
      f.write(result)

  return result


def truncate_names(hits, outfname=None):
  "Truncate all sequence names to the first word (separated by whitespace) only"
  if isinstance(hits, str):
    hits = Ali(hits, fasta_mode=True)
  for eg in hits:
    eg.code = eg.code.split()[0]
  result = hits.toFastaString()
  if outfname:
    with open(outfname, "w") as f:
      f.write(result)
  return result

