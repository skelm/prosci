#!/usr/bin/env python
from __future__ import print_function
import sys
import os
import subprocess

from prosci.common import write_file, read_file
from prosci.util.psa import annotate_surface_accessibility


SURFACEDIST_PATH = os.path.join(os.path.dirname(sys.argv[0]), "surfacedist")


def calculate_surface_distances(pdbfile, outprefix="", maxdist=None):
  "Run surfacedist to calculate the shortest distance along the protein surface between all pairs of surface residues. Requires the binaries 'psa' and 'surfacedist'."

  inf = float("inf")
  if maxdist is None:
    maxdist = inf

  if not outprefix:
    outprefix = os.path.splitext(pdbfile)[0]+"."

  solfile = os.path.splitext(pdbfile)[0]+".sol"

  #access = calculate_surface_accessibility_boolean(pdbfile, bcutoff=0.1)
  structure = annotate_surface_accessibility(pdbfile, bcutoff=0.1)

  # Calculate atom and residue distances in C++
  #
  surfdistpath = os.path.join(os.path.dirname(sys.argv[0]), "surfacedist")
  if not os.path.exists(surfdistpath):
    surfdistpath = "surfacedist"

  if os.path.exists(outprefix+"residue_surface_distances.txt"):
    return eval(read_file(outprefix+"residue_surface_distances.txt")), structure

  out = subprocess.Popen([surfdistpath, solfile], stdout=subprocess.PIPE, universal_newlines=True).communicate()[0]

  # Parse output of surfacedist
  surface_res_distances = {}
  prefix = "ShortestDist: "
  for line in out.splitlines():
    if line.startswith(prefix):
      a_from, a_to, dist_atom = eval(line[len(prefix):])
      if dist_atom > maxdist:
        continue
      r_from = a_from[:3]
      r_to = a_to[:3]
      surface_res_distances[(r_from, r_to)] = [dist_atom, a_from, a_to]
      surface_res_distances[(r_to, r_from)] = [dist_atom, a_to, a_from]
  write_file(outprefix+"residue_surface_distances.txt", repr(surface_res_distances))
  return surface_res_distances, structure

