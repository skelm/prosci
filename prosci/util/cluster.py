#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hierarchical clustering algorithm
#
# Author: Sebastian Kelm
# Created: 25/04/2011
#
from __future__ import print_function


class TreeNode(object):
  def __init__(self, data=None, a=None, b=None, simil=0):
    self.data = data
    self.a = a
    self.b = b
    self.simil = simil
  
  def has_children(self):
    return self.a is not None or self.b is not None
  
  def count_leaves(self):
    if not self.has_children():
      return 1
    
    leaves = 0
    if self.a is not None:
      leaves += self.a.count_leaves()
    if self.b is not None:
      leaves += self.b.count_leaves()
    
    return leaves
  
  def get_first_leaf(self):
    x=self
    while x.a is not None:
      x = x.a
    return x.data
  
  def get_leaves(self):
    output_list = []
    self._get_leaves(output_list)
    return output_list
  
  def _get_leaves(self, output_list=None):
    if self.has_children():
      self.a._get_leaves(output_list)
      self.b._get_leaves(output_list)
    else:
      output_list.append(self.data)
  
  def __str__(self):
    if data is not None:
      return str(self.data)
    else:
      return "{"+str(self.a)+":"+str(self.b)+"}"
  
  def __repr__(self):
    return "TreeNode(%s, %s, %s, %f)" % (repr(self.data), repr(self.a), repr(self.b), self.simil)


def hierarchical_clustering(objects, simil_matrix, mode="single", cutoff=None):
    assert objects
    assert simil_matrix and len(simil_matrix) == len(simil_matrix[0])
    assert mode in ("single", "average"), "Illegal clustering mode: "+mode
    
    
    clusters = []
    for o in objects:
      clusters.append(TreeNode(o))
    
    
    # Keep merging the two most similar clusters, until there is only one left
    #
    while len(clusters) > 1:
    
      max_simil=0  # similarity of the two most similar clusters
      a=1  # matrix indeces of the most similar clusters
      b=0  # matrix indeces of the most similar clusters
      
      
      # Find the most similar pair of clusters
      #
      # NOTE: j is always smaller than i. Thus, b is always smaller than a
      #
      for i in range(1, len(clusters)):
        for j in range(i):
          if simil_matrix[i][j] > max_simil:
            max_simil = simil_matrix[i][j]
            a=i
            b=j
      
      
      if cutoff is not None and max_simil < cutoff:
        # Stop clustering, when we have clusters left that are less similar than a given cutoff
        break;
      
      
      # Merge the two clusters
      #
      newc = TreeNode(a=clusters[b], b=clusters[a], simil=max_simil)
      clusters[b] = newc
      del clusters[a]
      
      
      # Delete matrix rows & columns for the old clusters
      # Make a new matrix row & column for the merged cluster
      #
      if mode == "single":
          # Replace matrix row b with the "minimum distance" or "maximum similarity" values from rows a and b
          for i in range(len(simil_matrix)):
            simil_matrix[i][b] = simil_matrix[b][i] = max(simil_matrix[b][i], simil_matrix[a][i])
      elif mode == "average":
          # Replace matrix row b with the "average distance" or "average similarity" values from rows a and b
          size_a = newc.a.count_leaves()
          size_b = newc.b.count_leaves()
          for i in range(len(simil_matrix)):
            simil_matrix[i][b] = simil_matrix[b][i] = (simil_matrix[b][i] * size_b + simil_matrix[a][i] * size_a) / float(size_a + size_b)
      
      
      # Delete matrix row a, moving the following matrix rows up
      del simil_matrix[a]
      
      # Delete matrix column a, moving the following matrix columns left
      for i in range(len(simil_matrix)):
        del simil_matrix[i][a]
    
    
    assert clusters, "No clusters left... something went wrong"
    
    return clusters
    

################################################################################


import sys
import os
from collections import defaultdict
import pickle as pickle

import numpy
import scipy.cluster.hierarchy
from scipy.spatial.distance import squareform

def cluster_items(items, cutoff, f_dist=None, f_weight=None, dist_mat=None, method="single", dist_mat_file=None):
  "Cluster annotated residues by their location in space and return annotation for the largest cluster"
  
  assert len(items) > 1
  
  # Try loading distance matrix from a file
  #
  if dist_mat_file is not None:
    if isinstance(dist_mat_file, str):
      if os.path.exists(dist_mat_file):
        with open(dist_mat_file, "rb") as f:
          dist_mat = pickle.load(f)
    else:
        dist_mat = pickle.load(dist_mat_file)
    assert dist_mat
    print("Loaded dist_mat from file")
  
  # Make distance matrix if it doesn't exist
  #
  if dist_mat is None:
    assert f_dist is not None, "Must give a dist_mat or an f_dist, both must not be None"
    dist_mat = numpy.zeros((len(items), len(items)))
    for i, k1 in enumerate(items):
      for j in range(i+1, len(items)):
        k2 = items[j]
        try:
          d = f_dist(k1, k2)
        except KeyError:
          print("No distance available between residues:", k1, k2)
          d = float("inf")
        dist_mat[i][j] = dist_mat[j][i] = d
    dist_mat = squareform(dist_mat, force="tovector")
    if dist_mat_file and isinstance(dist_mat_file, str):
      with open(dist_mat_file, "wb") as f:
        pickle.dump(dist_mat, f)    
  '''
  elif dist_mat_file is not None:
    if isinstance(dist_mat_file, basestring):
      with open(dist_mat_file, "rb") as f:
        dist_mat = pickle.load(f)
    else:
        dist_mat = pickle.load(dist_mat_file)
  '''
  
  if f_weight is None:
    f_weight = lambda x: 1
  
  #print "Cutoff for clustering method", method, "is", cutoff
  link_mat = scipy.cluster.hierarchy.linkage(dist_mat, method=method) # method: "single" or "average"
  clusters = scipy.cluster.hierarchy.fcluster(link_mat, cutoff, criterion='distance')
  
  cluster_choice = defaultdict(lambda:0)
  #print "Cluster assignments:"
  for i, cl in sorted(enumerate(clusters), key=lambda x: x[1]):
    cluster_choice[cl] += f_weight(items[i]) # weight items for cluster size ranking (e.g. to combat redundancy)
  
  '''
  dendro = scipy.cluster.hierarchy.dendrogram(link_mat, color_threshold=cutoff)
  ## Fix for broken y axis limits
  #
  dendro_ymax = 1+max([x for lst in dendro['dcoord'] for x in lst if x < inf])
  plt.gca().set_ylim(0, dendro_ymax)
  #
  ##
  
  plt.savefig(args.outprefix+'dendrogram.png')
  plt.close()
  '''
  
  cluster_choice = sorted(list(cluster_choice.items()), key=lambda x: -x[1])
  cluster_rank = {}
  for i, (cl, score) in enumerate(cluster_choice):
    cluster_rank[cl] = i+1
    #print cl, ":", score
  #print
  
  cluster_color_annotation = [0] * len(items)
  topcluster = cluster_choice[0][0]
  for i, cl in enumerate(clusters):
    if cl:
      cluster_color_annotation[i] = 1 + len(cluster_rank) - cluster_rank[cl]
    else:
      cluster_color_annotation[i] = 0
  
  return list(clusters), cluster_choice, cluster_rank, cluster_color_annotation


def seqnonid(seq1, seq2):
  "Get the number of non-identical residues in a pair of raw same-length sequences"
  n=0
  for a, b in zip(seq1, seq2):
    if a == b:
      n += 1
  #return float(n) / len(seq1)
  return len(seq1) - n



### NOSE TESTS


def test_cluster_items():
  seqs = """
  VLGG
  VRGG
  VRTT
  YCAR
  YCTS
  YCTT
  """.split()
  
  clusters, cluster_choice, cluster_rank, cluster_color_annotation = cluster_items(seqs, 3, seqnonid, method="average")

  assert clusters == [2, 2, 2, 1, 1, 1]
  assert cluster_choice  == [(1, 3), (2, 3)]
  assert cluster_rank == {1: 1, 2: 2}
  assert cluster_color_annotation == [1, 1, 1, 2, 2, 2]


if __name__ == "__main__":
  test_cluster_items()

