#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os, tempfile

from prosci.common import ParsingError
#from prosci.util.gaps import length_ungapped
from prosci.util.pdb import Pdb
from prosci.util.pdb3d import superimpose as pdb3dsuperimpose


def bangalign(pdb1, pdb2, opts=""):
    assert type(pdb1) == type(pdb2)
    if type(pdb1) == Pdb:
      return bangalign_objects(pdb1, pdb2, opts)
    else:
      return bangalign_files(pdb1, pdb2, opts)


def bangalign_files(pdb1, pdb2, opts=""):
    f = os.popen("bang %s %s %s"%(opts, pdb1, pdb2))
    result = f.readlines()
    f.close()
    
    #print "".join(result)

    info=""
    query=""
    match=""
    for i, line in enumerate(result):
      fields = line.split("=", 1)
      if fields[0] == "Query":
        query = fields[1]
      elif fields[0] == "Match":
        match = fields[1]
      elif not line.startswith("#"):
        info += line
    if not query or not match:
      raise ParsingError("Could not parse Bang output. No sequences???")
    
    info_dict = {}
    for line in info.splitlines():
        if "=" in line:
            k, v = line.split("=", 1)
            if k == "Equivalence":
                v = [float(x) for x in v.split(",")]
            elif k in ["Query length", "Match length"]:
                v = int(v)
            else:
                try:
                    v = float(v)
                except ValueError:
                    pass
            info_dict[k] = v
    return query.strip(), match.strip(), info_dict


def bangalign_objects(pdb1, pdb2, opts=""):
    f1 = tempfile.NamedTemporaryFile()
    f1.write(str(pdb1))
    f1.flush()
    
    f2 = tempfile.NamedTemporaryFile()
    f2.write(str(pdb2))
    f2.flush()
    
    query, match, info = bangalign_files(f1.name, f2.name, opts)

    f1.close()
    f2.close()
    
    return query, match, info




def superimpose(struc1_allchains, struc2_allchains, subset1=None, subset2=None, fname1=None, fname2=None, align_atoms=["N", "CA", "C", "O"], options="", modify_structures=True):
    assert type(struc1_allchains) == type(struc2_allchains)
    assert type(subset1) == type(subset2)
    
    pdb1_filename=fname1
    pdb2_filename=fname2
    
    if not isinstance(struc1_allchains, Pdb):
        pdb1_filename = struc1_allchains
        pdb2_filename = struc2_allchains
        struc1_allchains = Pdb(pdb1_filename)
        struc2_allchains = Pdb(pdb2_filename)
        modify_structures=False # We're not returning the structures to the caller, so no use modifying them
    
    if None == subset1:
        subset1 = struc1_allchains
    if None == subset2:
        subset2 = struc2_allchains
    
    
    # if structure has more than 1 chain, only use the first one
    if subset1.chaincount() > 1:
      subset1 = subset1.get_first_chain()
    if subset2.chaincount() > 1:
      subset2 = subset2.get_first_chain()
    
    
    # align structures and get the sequence alignment
    if pdb1_filename and pdb2_filename:
        # This will run TMalign on the original structure files.
        seq1, seq2, alignment_info = bangalign_files(pdb1_filename, pdb2_filename, options)
    else:
        # This will create temporary PDB files and run the alignment program on those.
        seq1, seq2, alignment_info = bangalign_objects(subset1, subset2, options)
    
    try:
        rmsd_value = pdb3dsuperimpose(struc1_allchains, struc2_allchains, seq1, seq2, subset1, subset2, align_atoms, modify_structures)
    except:
        print("Seq1", seq1)
        print("ngap", seq1.replace("-", ""))
        print("Str1", struc1_allchains.get_seq())
        print()
        print("Seq2", seq2)
        print("ngap", seq2.replace("-", ""))
        print("Str2", struc2_allchains.get_seq())
        
        raise
    return rmsd_value, seq1, seq2, alignment_info

if __name__ == "__main__":
    rmsd_value, seq1, seq2, alignment_info = superimpose("test/1prn.atm", "test/2por.atm")
    print(rmsd_value)
    print(seq1)
    print(seq2)
    print(alignment_info)
