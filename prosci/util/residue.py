#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Residue class represents a single amino acid in a PDB file.
#
# Author: Sebastian Kelm
# Created: 07/05/2009
#
# Revisions:
# 07/05/2009    Sebastian Kelm    Moved from mescol.py into its own script, and generalised to all-atom data.
#
from __future__ import print_function

import copy
import math
from collections import OrderedDict

import numpy

from prosci.common import OverridableField, SettableField, ReadOnlyField, write_file
from prosci.util.pdb import Pdb, Atom, residueLetter, residueCode
from prosci.util.pdb3d import dihedral_angle as _dihedral_angle
from prosci.util.gaps import deGappify, isGap


def _get_backbone_dihedrals(residues, i):
  "Get the Phi and Psi angles for a given index in the given ResidueList"

  r2 = residues[i]
  if r2 is None:
    return (None, None)
  if None in (r2.N, r2.CA, r2.C):
    return (None, None)

  if i == 0:
    r1 = None
  else:
    r1 = residues[i-1]
    if not r1.is_left_neighbour_of(r2) or r1.C is None:
      r1 = None

  if i >= len(residues)-1:
    r3 = None
  else:
    r3 = residues[i+1]
    if not r2.is_left_neighbour_of(r3) or r3.N is None:
      r3 = None

  if r1 is None:
    phi = None
  else:
    phi = _dihedral_angle(r2.N.xyz  - r1.C.xyz, r2.CA.xyz - r2.N.xyz, r2.C.xyz - r2.CA.xyz)
  if r3 is None:
    psi = None
  else:
    psi = _dihedral_angle(r2.CA.xyz - r2.N.xyz, r2.C.xyz - r2.CA.xyz, r3.N.xyz - r2.C.xyz)

  return (phi, psi)


class SequenceMappingError(Exception):
  pass

class AtomTypeNotFoundError(ValueError):
  pass

class DuplicateResidueError(ValueError):
  pass

################################################################################


class BaseResidueList(list):
  "A list of BaseResidue objects."

  chain = SettableField("chain")
  code = OverridableField("chain")

  def __init__(self, pdb, code=None, cryst1="", filename=""):
    "Takes a list or tuple or Pdb object as argument. Otherwise passes the argument to Pdb() first."
    self.cryst1 = cryst1
    self.filename = filename
    if not pdb:
      pass
    elif (isinstance(pdb, list) or isinstance(pdb, tuple)) and isinstance(pdb[0], BaseResidue):
      for r in pdb:
        self.append(r)
    else:
      if not isinstance(pdb, Pdb):
        pdb = Pdb(pdb)
      for r in pdb.xresidues():
        self.append(BaseResidue(r))
      if pdb.code:
        self.code = pdb.code
      try:
        if not self.cryst1:
          self.cryst1 = pdb.cryst1
      except AttributeError:
        pass
      try:
        if not self.filename:
          self.filename = pdb.filename
      except AttributeError:
        pass
    if code is not None:
      self.code = code


  def sort_residues(self):
    self.sort(key=lambda x: x.get_id_tuple())


  def write(self, fname):
    write_file(fname, str(self))
  
  def delete_residues_before_1(self):
    "Get rid of residues before number 1 (typically HIS tags etc.)"
    while len(self) > 0 and self[0].ires < 1:
      del self[0]
  
  def delete_residues_without_CA(self):
    "Get rid of residues that don't have a CA atom"
    for i in range(len(self)-1, -1, -1):
      if self[i].CA is None and self[i].get_seq() != "X":
        del self[i]
  
  def __repr__(self):
    return "BaseResidueList(%s, cryst1=%s, filename=%s)"%(list.__repr__(self), repr(self.cryst1), repr(self.filename))

  def __str__(self):
    out=self.cryst1
    for r in self:
      if r:
        out += str(r)
    return out + "TER\n"

  def __getslice__(self, start=None, end=None):
    return BaseResidueList(list.__getslice__(self, start, end), cryst1=self.cryst1, filename=self.filename)

  def __add__(self, other):
    return BaseResidueList(list.__add__(self, other))
  def __radd__(self, other):
    return BaseResidueList(list.__add__(other, self))
  def __iadd__(self, other):
    self.extend(other)
    return self


  def get_seq(self):
    "Returns a FASTA string representation of the sequence."
    seq = ""
    for r in self:
      if r is not None:
        seq += r.get_seq()
    return seq


  def get_gapped_seq(self, gapchar='-', collapse_gaps=False):
    "Returns a FASTA string representation of the sequence, with gaps in the residue numbering represented by special characters (default: '-')."
    seq = ""
    prevres = None
    for r in self:
      if r is not None:
        if prevres and r.ires > prevres.ires + 1:
          if collapse_gaps:
            seq+=gapchar
          else:
            for i in range(r.ires - prevres.ires - 1):
              seq.append(gapchar)
        seq+=r.get_seq()
      else:
        seq+=gapchar
      prevres = r
    return seq


  def get_coords(self, atomfilter=lambda atom: True):
    "Get a numpy array of coordinates"
    coords=[]
    for r in self:
      if r is not None:
        for a in r:
          if atomfilter(a):
            coords.append(a.xyz)
    return numpy.array(coords)


  def deep_copy(self):
    return copy.deepcopy(self)

  def get_gap_list(self, ignore_residue_numbers=False, exclude_chain_breaks=True):
    """Returns a list of gaps, as determined using residue numbers. None elements count as missing residues.

    Format: [[index, gaplength], ...] , where index is the array index of the gap and gaplength is the number of missing residues."""
    results=[]

    start=-1
    for i, r in enumerate(self):
      if r is None:
        if start < 0:
          start = i
      elif start >= 0:
        results.append([start, i])
        start = -1
    if start >= 0:
      results.append([start, len(self)])

    if not ignore_residue_numbers:
      len_before = len(results)
      for i in range(1, len(self)):
        if self[i-1] is not None and self[i] is not None:
          if exclude_chain_breaks and self[i].chain != self[i-1].chain:
            continue
          gaplen = self[i].ires - self[i-1].ires - 1
          if gaplen > 0:
            results.append([i, gaplen])
      if len_before and len_before != len(results):
        results.sort()

    return results


  def to_pdb(self, atomfilter=lambda atom: True):
    "Returns a Pdb object containing the Atom objects within this BaseResidueList"
    p = Pdb(self.code, [])
    for res in self:
      for atm in res:
        if atomfilter(atm):
          p.data.append(atm)
    return p


  def renumber_atoms(self, start=1):
    "Renumbers all atoms, starting from 1 upwards. A different starting number can be set using the 'start' option."
    i = start
    for r in self:
      for a in r:
        a.iatom = i
        i += 1

  def renumber(self, start=1):
    "Renumbers all residues, starting from 1 upwards. A different starting number can be set using the 'start' option."
    for i, r in enumerate(self):
      r.ires = i+start
      r.inscode = ""

  def iter_backbone(self):
    "Iterate over all backbone atoms that aren't None"
    for r in self:
      for a in r.iter_backbone():
        yield a


  def split_chains(self):
    "Returns a list of BaseResidueList objects, split at the positions where chain codes differ between subsequent residues."
    chains = [BaseResidueList([])]
    for r in self:
      if r is None:
        continue
      if chains[-1] and chains[-1][-1].chain != r.chain:
        chains.append(BaseResidueList([]))
      chains[-1].append(r)
    for c in chains:
      c.code = self.code
    return chains


  def remove_residues(self, func):
    i=0
    while i<len(self):
      if func(self[i]):
        del self[i]
      else:
        i += 1


  def find_residue(self, ires, inscode="", chain="", fuzzy=True, reverse=False):
    instring = None
    if isinstance(ires, BaseResidue) or isinstance(ires, Atom):
      inscode = ires.inscode
      chain = ires.chain
      ires = ires.ires
    elif isinstance(ires, str):
      instring = ires
      if ires[:1].isalpha():
        chain = ires[:1]
        ires = ires[1:]
      if ires[-1:].isalpha():
        inscode = ires[-1:]
        ires = ires[:-1]
      ires = int(ires)
    elif not isinstance(ires, int) and len(ires) in (2, 3) and not inscode and not chain:
      if len(ires) == 2:
        chain, ires = ires
      else:
        chain, ires, inscode = ires

    if reverse:
      iterator = range(len(self)-1, -1, -1)
    else:
      iterator = range(len(self))
    result = -1
    for i in iterator:
      res = self[i]
      if (res.ires == ires) and (inscode == res.inscode):
        if (not chain) or (chain == res.chain):
          result = i
          break

    if fuzzy and result < 0 and instring is not None:
      try:
        result = self.find_residue(instring[1:], chain=instring[:1], fuzzy=False, reverse=reverse)
      except ValueError as e:
        print(instring, file=sys.stderr)
        raise

    return result


  def map_to_seq(self, seq, renumber=False, start_number=1):
    model_residues = self

    modelseq = model_residues.get_seq()
    assert len(modelseq) == len(model_residues)

    model_fragments = [BaseResidueList([model_residues[0]])]
    for i in range(1,len(model_residues)):
      if is_residue_consecutive(model_residues[i-1], model_residues[i]) and is_residue_numbering_consecutive(model_residues[i-1], model_residues[i]):
        model_fragments[-1].append(model_residues[i])
      else:
        model_fragments.append(BaseResidueList([model_residues[i]]))

    def find_frag(seq, fragseq, i):
      n = 0
      while i < len(seq):
        n = get_common_frag(fragseq, seq[i:])
        if n >= len(fragseq):
          return i
        i += 1
      return -1

    def get_common_frag(fragseq, seq):
      n=0
      for c1, c2 in zip(fragseq, seq):
        if c1 == c2 or "X" in (c1, c2):
          n += 1
        else:
          break
      return n

    seq = seq.upper()
    gapped_seq = seq
    seq = deGappify(seq)
    #modelseq_aligned = ""
    indices = []
    i = 0
    ifrag = 0
    while ifrag < len(model_fragments):
      frag = model_fragments[ifrag]
      fragseq = frag.get_seq()
      j = find_frag(seq, fragseq, i)
      if j < 0:
        common_frag = frag[:get_common_frag(fragseq, seq[i:])]
        if not common_frag:
          raise SequenceMappingError("Cannot map structure to sequence:\n%s\n%s\n%s"%(modelseq, seq, str([f.get_seq() for f in model_fragments])))
        else:
          model_fragments[ifrag] = common_frag
          model_fragments.insert(ifrag+1, frag[len(common_frag):])
          continue

      indices.extend(list(range(j, j+len(fragseq))))
      #modelseq_aligned += "-" * (j-i) + fragseq
      i = j + len(fragseq)

      if renumber:
        # Renumber residues to show their position in the native sequence
        # (starting with residue 1)
        #
        for k, residue in enumerate(frag):
          for atom in residue:
            atom.ires = start_number + j + k
            atom.inscode = ""
      ifrag += 1

    #modelseq_aligned += "-" * (len(seq) - len(modelseq_aligned))

    # Add gaps
    if len(seq) != len(gapped_seq):
      # Make a cumulative gap count at each position of the ungapped sequence
      gapcounts = []
      n = 0
      for c in gapped_seq:
        if isGap(c):
          n += 1
        else:
          gapcounts.append(n)
      for i, v in enumerate(indices):
          indices[i] = v + gapcounts[v]
      try:
        for i, res in enumerate(self):
          assert res.get_seq() == gapped_seq[indices[i]]
      except:
        print(self.get_seq(), file=sys.stderr)
        print("".join([gapped_seq[x] for x in indices]), file=sys.stderr)
        print(res.get_seq(), file=sys.stderr)
        print(gapped_seq[indices[i]], file=sys.stderr)
        raise

    return indices


  def get_backbone_dihedrals(self):
    "Return the Phi and Psi angles for all residues in this BaseResidueList (or None where it cannot be calculated)"
    angles = []
    for i in range(len(self)):
      a = _get_backbone_dihedrals(self, i)
      angles.append(a)
    return angles


################################################################################


class BaseResidue(object):
  "Class representing a single amino acid. Has fields for all main chain and CB atoms. Remaining atoms are in the list self.rest"

  res = SettableField("res")
  ires = SettableField("ires")
  inscode = SettableField("inscode")
  chain = SettableField("chain")
  hetatm = SettableField("hetatm")
  altloc = ReadOnlyField("altloc")


  def __init__(self, atoms=None):
    "Expects a list of Atom objects as input, or a Pdb object."

    self.N = None
    self.CA = None
    self.C = None
    self.O = None
    self.CB = None
    self.rest = []

    if atoms is not None:
      for a in atoms:
        if a is None:
          continue
        self.add(a)

      #if self.res in ("GLY", "ALA"):
      # del self.rest[:]
      # if self.res == "GLY":
      #   self.CB = None

  def __bool__(self):
    return bool(self.N or self.CA or self.C or self.O or self.CB or self.rest)

  def __iter__(self):
    if self.N is not None:
      yield self.N
    if self.CA is not None:
      yield self.CA
    if self.C is not None:
      yield self.C
    if self.O is not None:
      yield self.O
#   if self.CA.res != "GLY":
    if self.CB is not None:
      yield self.CB
    for a in self.rest:
      yield a


  def add(self, a):
    if not isinstance(a, Atom):
      a = Atom(a)
    if a.atom in ("N", "CA", "C", "O", "CB"):
      curratm = getattr(self, a.atom)
      # Only save atom with lowest altloc
      if curratm is None or curratm.altloc < a.altloc:
        setattr(self, a.atom, a)
    else:
      # TODO: do something about multiple altlocs
      self.rest.append(a)


  def iter_backbone(self):
    "Iterate over all backbone atoms that aren't None"
    if self.N is not None:
      yield self.N
    if self.CA is not None:
      yield self.CA
    if self.C is not None:
      yield self.C
    if self.O is not None:
      yield self.O


  def iter_sidechain(self):
    "Iterate over all side chain atoms"
    if self.CB is not None:
      yield self.CB
    for a in self.rest:
      yield a

  def __str__(self):
    s=""
    for a in self:
      s+=str(a)
    return s


  def __repr__(self):
    return "BaseResidue(%s)"%(repr([self.N, self.CA, self.C, self.O, self.CB]+self.rest))


  def get_id(self):
    "Get an identifier string that should be unique to each residue in a protein"
    return "%s%d%s" % (self.chain, self.ires, self.inscode)

  def get_id_tuple(self):
    "Get an identifier tuple that should be unique to each residue in a protein"
    return (self.chain, self.ires, self.inscode)

  def get_seq(self):
    "Get the amino acid letter representing this BaseResidue"
    return residueLetter(self.res)

  def has_CA(self):
    "Check if this residue has a CA atom"
    return self.CA is not None

  def get_atom(self, atype):
    "Get atom of a specified type, if it exists. Else raise ValueError."
    atom = None
    if atype in ("N", "CA", "C", "O", "CB"):
      try:
        atom = getattr(self, atype)
      except AttributeError:
        pass
    else:
      for a in self.rest:
        if a.atom == atype:
          atom = a
          break

    if atom is not None:
      return atom

    raise AtomTypeNotFoundError("BaseResidue '%s/%d%s/%s' does not contain an atom of type '%s'" % (self.chain, self.ires, self.inscode, self.res, atype))


  def set_type(self, newtype):
    """Set a new residues type.

    Deletes the side chain unless newtype == self.type. Keeps CB unless newtype == 'GLY'."""

    if len(newtype) == 1:
      newtype = residueCode(newtype)

    if newtype == self.res:
      isok = True
      for a in self:
        if a.res != newtype:
          isok = False
          break
      if isok:
        rebuild_CB(self)
        return
    
    assert len(newtype) == 3
    assert newtype.isupper()

    if newtype == "GLY":
      self.CB = None
    del self.rest[:]

    #print "Relabeling residue (%s.%d%s) from %s to %s" % (self.chain, self.ires, self.inscode, self.res, newtype)

    self.res = newtype
    for a in self:
      assert a.res == newtype
    
    rebuild_CB(self)


  def copy(self):
    "Make a copy of this object."
    return copy.deepcopy(self)

  def equals_in_name(self, other):
    return self.chain == other.chain and self.ires == other.ires and self.res == other.res and self.inscode == other.inscode

  def is_left_neighbour_of(self, other, minlen_sq=1.0, maxlen_sq=4.0, numbering=False):
    return is_residue_consecutive(self, other, minlen_sq, maxlen_sq) and (not numbering or is_residue_numbering_consecutive(self, other))


  def is_right_neighbour_of(self, other, minlen_sq=1.0, maxlen_sq=4.0, numbering=False):
    return is_residue_consecutive(other, self, minlen_sq, maxlen_sq) and (not numbering or is_residue_numbering_consecutive(other, self))
  
  
  def rmsd(self, other):
    "Measure RMSD between two Residues. They must be of the same type and only atoms with the same labels will be compared."
    atms_self = [(a.atom, a) for a in self if a.xyz is not None]
    atms_other = dict([(a.atom, a) for a in other if a.xyz is not None])
    R = 0.0
    N = 0
    for atom, a in atms_self:
      if atom in atms_other:
        R += a.dist(atms_other[atom])**2
        N += 1
    if not N:
      return 0.0
    return math.sqrt(R / N)



################################################################################



def is_residue_consecutive(res_a, res_b, minlen_sq=1.0, maxlen_sq=4.0):
    a = res_a.C
    b = res_b.N
    if a is None or b is None:
      return False
    d_sq = (a.x-b.x)**2 + (a.y-b.y)**2 + (a.z-b.z)**2
    if (d_sq < minlen_sq or d_sq > maxlen_sq):
      return False
    return True

def is_residue_numbering_consecutive(res_a, res_b):
  if res_a.chain != res_b.chain:
    return False
  if res_a.ires == res_b.ires - 1 and not res_b.inscode:
    return res_b.inscode == ""
  if res_a.ires == res_b.ires:
    if not res_a.inscode or not res_b.inscode:
      return (res_a.inscode == "") and (res_b.inscode == "A")
    return ord(res_a.inscode) == ord(res_b.inscode) - 1
  return False


cb_dictionary = {'CYS': [-0.3494607343363032, 0.79155529324285911, 0.49818968247044454, 1.5296357057135819], 'ASP': [-0.34996888099893109, 0.78680052199417105, 0.50545821158245419, 1.5318055409680786], 'SER': [-0.34908005233451428, 0.79092525569532701, 0.50008816225520469, 1.5298749212857365], 'GLN': [-0.35044189807274562, 0.78789868002634589, 0.50391579356824856, 1.5307608842551144], 'LYS': [-0.35014881263058767, 0.78862863785937809, 0.50338903814841085, 1.5311461087301732], 'ILE': [-0.36317297924619868, 0.78202773684664761, 0.50393645313205515, 1.5451861881417324], 'ASN': [-0.34954774900564267, 0.78213004101991257, 0.5126836623595018, 1.5321433083090938], 'THR': [-0.36088247834694348, 0.787447497345662, 0.49708105577718109, 1.5401087763522436], 'PHE': [-0.35040956754722258, 0.78912631290685253, 0.50162366982130213, 1.5339592272837324], 'ALA': [-0.34777316024597776, 0.78875689088188949, 0.50566390742614242, 1.5253623347586374], 'MET': [-0.35070773060772492, 0.78911407016265778, 0.50143527870114502, 1.530863273877535], 'HIS': [-0.3478745720232258, 0.78875770121288913, 0.50381793689150922, 1.53204409902269], 'LEU': [-0.34927313668815713, 0.7911706392471316, 0.49990908813589296, 1.5313231292876908], 'ARG': [-0.35073600249773451, 0.78838749120654794, 0.50304768909818021, 1.5309172528392483], 'TRP': [-0.3496482864883515, 0.78907538478814376, 0.50231116231957951, 1.5332689020009584], 'PRO': [-0.22910526303159487, 0.84414806305461676, 0.48310967532320609, 1.5324482100664505], 'VAL': [-0.36481633408183051, 0.78221455833922093, 0.50264044284361142, 1.5453102405120656], 'GLU': [-0.35008028783676887, 0.78714165176127382, 0.50553324134098787, 1.5316362178823235], 'TYR': [-0.34979587900927023, 0.78983362800133428, 0.5011455140152965, 1.5342036619521204]}


cb_dictionary['UNK'] = cb_dictionary['XXX'] = cb_dictionary['ALA']


def rebuild_CB(res, replace=False, ignore_glycine=True):
  "Rebuild a given BaseResidue's missing C-beta atom co-ordinates"

  if not (res.CA and res.C and res.N):
    # Can't build CB
    #print "Missing main chain atoms - CA,C,N needed to rebuild CB"
    return False

  if res.CB and not replace:
    # There is already a CB atom present and we were told not to replace it
    #print "CB already present"
    return False

  if res.res == "GLY" and ignore_glycine:
    # Glycines don't have CB atoms
    #print "Ignoring glycine"
    return False

  CB = res.CA.copy()
  CB.atom = "CB"

  n_direction = res.N.xyz - res.CA.xyz
  n_direction /= numpy.linalg.norm(n_direction)
  c_direction = res.C.xyz - res.CA.xyz
  c_direction /= numpy.linalg.norm(c_direction)
  norm_direction = numpy.cross(n_direction, c_direction)
  norm_direction /= numpy.linalg.norm(norm_direction)
  norm2_direction = numpy.cross(n_direction, norm_direction)
  norm2_direction /= numpy.linalg.norm(norm_direction)

  n_factor, norm_factor, norm2_factor, cb_dist = cb_dictionary[residueCode(residueLetter(res.res))]

  cb_vector = (n_direction * n_factor) + (norm_direction * norm_factor) + (norm2_direction * norm2_factor)
  cb_vector /= numpy.linalg.norm(cb_vector)
  cb_vector *= cb_dist

  CB.xyz += cb_vector

  res.CB = CB
  return True


################################################################################
# Residue class represents a single amino acid in a PDB file.
#
# Author: Sebastian Kelm
# Created: 07/05/2009
#
# Revisions:
# 07/05/2009    Sebastian Kelm    Moved from mescol.py into its own script, and generalised to all-atom data.
#

ALTLOCS = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']


class Residue(BaseResidue):
  "Class representing a single amino acid, along with all its alternative locations."
  
  
  class AtomGetter(object):
    "Descriptor to access an EntryGroup's master entry"
    def __init__(self, field, default=None):
      self.f = field
      self.default = default
    def __get__(self, obj, cls=None):
        if not obj.alt:
          return self.default
        # Try to get the requested atom from the first altloc, and if it is None,
        # try the next altloc until we find something that isn't None.
        #
        if "" in obj.alt:
          val = getattr(obj.alt[""], self.f)
          if val is not None:
            return val
        if "A" in obj.alt:
          val = getattr(obj.alt["A"], self.f)
          if val is not None:
            return val
        k = min(obj.alt)
        return getattr(obj.alt[k], self.f)
    def __set__(self, obj, val): 
        if not obj.alt:
          raise ValueError("Residue has no contents, cannot set field %s to %s" % (str(self.f), str(val)))
        if "" in obj.alt:
          k = ""
        elif "A" in obj.alt:
          k = "A"
        else:
          k = min(obj.alt)
        return setattr(obj.alt[k], self.f, val)
    def __delete__(self, obj): 
        raise AttributeError("Illegal operation: cannot delete attribute")
  
  N = AtomGetter("N")
  CA = AtomGetter("CA")
  C = AtomGetter("C")
  O = AtomGetter("O")
  CB = AtomGetter("CB")
  rest = AtomGetter("rest")
  
  
  def __init__(self, atoms=None, iteralt=True):
    "Expects an iterable of Atom objects as input, or a Pdb object."
    
    #super(Residue, self).__init__()
    
    self.alt = OrderedDict()
    self.iteralt = iteralt
    
    if atoms is not None:
      for a in atoms:
        if a is None:
          continue
        self.add(a)
  
  
  def add(self, a):
    if not isinstance(a, Atom):
      a = Atom(a)
    r = self.alt.get(a.altloc, BaseResidue())
    r.add(a)
    self.alt[a.altloc] = r
  
  
  def __iter__(self):
    for k in self.alt:
      r = self.alt[k]
      for a in r:
        yield a
  
  
  def iter(self, alt=None):
    "Iterate over all atoms that aren't None"
    if alt is None:
      alt = self.iteralt
    for k in self.alt:
      r = self.alt[k]
      for a in r:
        yield a
      if not alt:
        break
  
  
  def iter_backbone(self, alt=None):
    "Iterate over all backbone atoms that aren't None"
    if alt is None:
      alt = self.iteralt
    for k in self.alt:
      r = self.alt[k]
      for a in r.iter_backbone():
        yield a
      if not alt:
        break
      
  
  def iter_sidechain(self, alt=None):
    "Iterate over all side chain atoms"
    if alt is None:
      alt = self.iteralt
    for k in self.alt:
      r = self.alt[k]
      for a in r.iter_sidechain():
        yield a
      if not alt:
        break
  
  
  def __repr__(self):
    return "Residue(%s)"%(repr(list(self)))
  
  
  #def __getitem__(self, alt):
  # return self.alt[alt]
  
  
  def has_alt(self):
    return len(self.alt) > 1
  
    
  def set_type(self, newtype):
    """Set a new residues type.
    
    Deletes the side chain unless newtype == self.type. Keeps CB unless newtype == 'GLY'."""
    
    for res in list(self.alt.values()):
      res.set_type(newtype)
  
  
  def has_CA(self):
    "Check if this residue and any of its alternative locations have a CA atom"
    for alt in self.alt:
      if self.alt[alt].has_CA():
        return True
    return False



class ResidueList(BaseResidueList):
  "A list of Residue objects, allowing for alternative residue locations (e.g. loop decoys)."
  
  def __init__(self, pdb, code=None, iteralt=True, cryst1="", filename=""):
    "Takes a list or tuple or Pdb object as argument. Otherwise passes the argument to Pdb() first."
    self.iteralt = iteralt
    self.cryst1 = cryst1
    self.filename = filename
    
    resindex={}
    def addres(r):
      if r is None or isinstance(r, Residue):
        self.append(r)
        #if r is None:
        #  self.append(r)
        #elif r.get_id_tuple() not in resindex:
        #  self.append(r)
        #  resindex[r.get_id_tuple()] = r
        #else:
        #  raise DuplicateResidueError("Duplicate Residue added to ResidueList: %s" % (r.get_id()))
      elif r.get_id_tuple() not in resindex:
        r2 = Residue(r, iteralt=self.iteralt)
        self.append(r2)
        resindex[r.get_id_tuple()] = r2
        #print "Adding residue", r2.get_id()+"-"+r2.altloc
      else:
        r2 = resindex[r.get_id_tuple()]
        #print "Adding alternative residue", r.get_id()+"-"+r.altloc, "to", r2.get_id()+"-"+r2.altloc
        for a in r:
          r2.add(a)
        #print "Added alternative residue", r.get_id()+"-"+r.altloc, "to", r2.get_id()+"-"+r2.altloc
        #print
    
    if pdb:
      try:
        self.iteralt = pdb.iteralt
      except AttributeError:
        pass
      if (isinstance(pdb, list) or isinstance(pdb, tuple)) and isinstance(pdb[0], BaseResidue):
        for r in pdb:
          addres(r)
      else:
        if not isinstance(pdb, Pdb):
          pdb = Pdb(pdb, nofilter=True)
        for r in pdb.xresidues():
          #print r
          addres(Residue(r, iteralt=self.iteralt))
        if pdb.code:
          self.code = pdb.code
        try:
          if not self.cryst1:
            self.cryst1 = pdb.cryst1
        except AttributeError:
          pass
        try:
          if not self.filename:
            self.filename = pdb.filename
        except AttributeError:
          pass
    if code is not None:
      self.code = code
  
  
  def add(self, res):
    "Intelligently add a Residue to this ResidueList, merging residues with the same ID (allowing for different altloc)"
    i = self.find_residue(res)
    if i < 0:
      self.append(Residue(res, iteralt=self.iteralt))
    else:
      self[i].add(res)
  
  
  def split_chains(self):
    "Returns a list of ResidueList objects, split at the positions where chain codes differ between subsequent residues."
    chains = [ResidueList([])]
    for r in self:
      if r is None:
        continue
      if chains[-1] and chains[-1][-1].chain != r.chain:
        chains.append(ResidueList([]))
      chains[-1].append(r)
    for c in chains:
      c.code = self.code
    return chains
  
  
  def __repr__(self):
    return "ResidueList(%s, iteralt=%s, cryst1=%s, filename=%s)"%(list.__repr__(self), repr(self.iteralt), repr(self.cryst1), repr(self.filename))
  
  def __getslice__(self, start=None, end=None):
    return ResidueList(list.__getslice__(self, start, end), iteralt=self.iteralt, cryst1=self.cryst1, filename=self.filename)
  
  def __add__(self, other):
    return ResidueList(list.__add__(self, other), iteralt=self.iteralt)
  def __radd__(self, other):
    return ResidueList(list.__add__(other, self), iteralt=self.iteralt)
  def __iadd__(self, other):
    self.extend(other)
    return self


