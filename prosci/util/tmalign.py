#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

#import sys
import os
import tempfile
import subprocess

from prosci.common import ParsingError, ExecutableDependencyError # DependencyError, 
#from prosci.util.gaps import length_ungapped
from prosci.util.pdb import Pdb
from prosci.util.protein import Protein
from prosci.util.residue import ResidueList
from prosci.util.tempdir import TempDir
#from prosci.util.pdb3d import rmsd_static
from prosci.util.ali import Ali
#from prosci.util.pdb3d import superimpose as pdb3dsuperimpose

import numpy


class PoorSuperpositionError(RuntimeError):
  pass
class StructureTooShortError(PoorSuperpositionError):
  pass


def tmalign(pdb1, pdb2, opts=""):
    "Superimpose pdb1 onto pdb2 using TMalign"
    assert type(pdb1) == type(pdb2)
    if isinstance(pdb1, Pdb) or isinstance(pdb1, ResidueList) or isinstance(pdb1, Protein):
      return tmalign_objects(pdb1, pdb2, opts)
    else:
      return tmalign_files(pdb1, pdb2, opts)


tmalign_runs = [0]
def tmalign_files(pdb1, pdb2, opts=""):
    "Superimpose pdb1 onto pdb2 using TMalign"
    tmalign_runs[0] += 1
    fh_matrix, fname_matrix = tempfile.mkstemp()
    try:
      command = "TMalign %s %s -m %s %s"%(pdb1, pdb2, fname_matrix, opts)
      #print command
      p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
      tm_result, tm_errors = p.communicate()
      #print "TMalign run %d times now." % tmalign_runs[0]
      #print "TMalign command:"
      #print "TMalign %s %s -m %s %s"%(pdb1, pdb2, fname_matrix, opts)
      #print "TMalign output:"
      #print tm_result
      #print "TMalign errors:"
      #print tm_errors
      f = os.fdopen(fh_matrix)
      try:
        rotation_matrix = f.read()
      finally:
        f.close()
      #print "Rotation matrix:"
      #print rotation_matrix
      #print ""
      if p.returncode != 0 or tm_errors:
        L1 = len(ResidueList(pdb1))
        L2 = len(ResidueList(pdb2))
        if min(L1, L2) < 3:
          raise StructureTooShortError("TMalign cannot deal with structures shorter than 3 residues. inputs: %s (%d residues) %s (%d residues)\n" % (pdb1, L1, pdb2, L2))
        raise ExecutableDependencyError("TMalign was not found or produced errors. Return code = %d, error messages:\n%s\noutput:%s\ninputs: %s (%d residues) %s (%d residues)\n"%(p.returncode, str(tm_errors), str(tm_result), pdb1, L1, pdb2, L2))
    finally:
      os.remove(fname_matrix)
    return parse_tmalign_output(tm_result.splitlines(), rotation_matrix.splitlines())


def tmalign_objects(pdb1, pdb2, opts=""):
    "Superimpose pdb1 onto pdb2 using TMalign"
    if isinstance(pdb1, ResidueList) or isinstance(pdb1, Protein):
      pdb1 = pdb1.to_pdb()
    if isinstance(pdb2, ResidueList) or isinstance(pdb2, Protein):
      pdb2 = pdb2.to_pdb()
    
    with TempDir() as td:
      pdb1.filter(altloc=True)
      with td.open("struc1.pdb", "w") as f:
        pdb1.write_renumbered(f, clearaltloc=True)
      
      pdb2.filter(altloc=True)
      with td.open("struc2.pdb", "w") as f:
        pdb2.write_renumbered(f, clearaltloc=True)
      
      try:
        return tmalign_files(td.get_path("struc1.pdb"), td.get_path("struc2.pdb"), opts)
      except:
        #print >>sys.stderr, "v"*80
        #print >>sys.stderr, pdb1
        #print >>sys.stderr, "="*80
        #print >>sys.stderr, pdb2
        #print >>sys.stderr, "^"*80
        raise
      finally:
        td.delete()


def parse_tmalign_output(tm_result, matrix_lines):
    "Parse the text output from TMalign into a dictionary and a tuple of numpy matrices that can be used to transform the moving protein onto the reference protein."
    try:
      transform = [matrix_lines[2].split()[1:], matrix_lines[3].split()[1:], matrix_lines[4].split()[1:]]
      translate = numpy.array([float(t[0]) for t in transform])
      rotmat = numpy.array([[float(x) for x in t[1:]] for t in transform])
      transform = (translate, rotmat)
    except:
      print("Error parsing the following TMalign rotation matrix:", file=sys.stderr)
      print("\n".join(matrix_lines), file=sys.stderr)
      print("TMalign output was:", file=sys.stderr)
      print("\n".join(tm_result), file=sys.stderr)
      raise

    info={}
    seq1 = None
    seq2 = None
    for i, line in enumerate(tm_result):
      if line.startswith("Aligned"):
        fields = line.split(",")
        for f in fields:
          f_fields = f.split("=")
          k=f_fields[0].strip()
          v=f_fields[-1].strip()
          if "." not in v:
            info[k] = int(v)
          else:
            info[k] = float(v)
      elif line[0:4] == '(":"':
        # return sequence1, equivalence, sequence2
        # equivalence is a sequence of " ", "." and ":" chars, where ":" indicates that the residues are within 5 Angstroms of each other.
        seq1 = info["seq1"] = tm_result[i+1].rstrip("\n\r")
        info["equiv"] = tm_result[i+2].rstrip("\n\r")
        seq2 = info["seq2"] = tm_result[i+3].rstrip("\n\r")
      elif line.startswith("TM-score"):
        score, desc = line.split("=")[1].split(None, 1)
        score = float(score)
        if "Chain_1" in desc:
          info["TM-score (Chain_1)"] = score
        elif "Chain_2" in desc:
          info["TM-score (Chain_2)"] = score

    if not (seq1 and seq2):
      raise ParsingError("Could not parse TMalign output. No sequences???:\nseq1=%s\nseq2=%s\ntransform=%s\nTM-align output:\n%s"%(seq1, seq2, str(transform), "".join(tm_result)))

    if not "TM-score" in info:
      info["TM-score"] = info["TM-score (Chain_2)"]

    return transform, info



def transform_structure(struc, transform):
  "Transform a structure's 3D coordinates according to a given transformation object"

  if isinstance(struc, ResidueList) or isinstance(struc, Protein):
    struc = struc.to_pdb()

  #tr, rotmat = transform
  #origin = numpy.array([0.0,0.0,0.0])
  #for atm in struc:
  #  origin += atm.xyz
  #origin /= len(struc)
  #
  #for atm in struc:
  #  atm.xyz = numpy.dot(atm.xyz - origin, rotmat) + origin + tr

  t, u = transform

  for atm in struc:
    x, y, z = atm.xyz
    atm.x = t[0] + u[0,0] * x + u[0,1] * y + u[0,2] * z
    atm.y = t[1] + u[1,0] * x + u[1,1] * y + u[1,2] * z
    atm.z = t[2] + u[2,0] * x + u[2,1] * y + u[2,2] * z
  for atm in struc.ligands:
    x, y, z = atm.xyz
    atm.x = t[0] + u[0,0] * x + u[0,1] * y + u[0,2] * z
    atm.y = t[1] + u[1,0] * x + u[1,1] * y + u[1,2] * z
    atm.z = t[2] + u[2,0] * x + u[2,1] * y + u[2,2] * z


  # The above is the python translation of the following fortran code:
  #
  # Code for rotating Chain-1 from (x,y,z) to (X,Y,Z):
  # do i=1,L
  #   X(i)=t(1)+u(1,1)*x(i)+u(1,2)*y(i)+u(1,3)*z(i)
  #   Y(i)=t(2)+u(2,1)*x(i)+u(2,2)*y(i)+u(2,3)*z(i)
  #   Z(i)=t(3)+u(3,1)*x(i)+u(3,2)*y(i)+u(3,3)*z(i)
  # enddo





def superimpose(struc1_allchains, struc2_allchains, subset1=None, subset2=None, fname1=None, fname2=None, align_atoms=("N", "CA", "C", "O"), options="", modify_structures=True, normalise_by_first=False, return_transform=False):
    """Superimpose the first protein onto the second. If modify_structures=True and structure1 is a protein structure object, structure1 will be rotated/translated onto structure2."""

    #pdb1_filename=fname1
    #pdb2_filename=fname2

    if isinstance(struc1_allchains, str):
      modify_structures = False
      #pdb1_filename = struc1_allchains
      # We're not returning the structures to the caller, so no use modifying them

    #if isinstance(struc2_allchains, str):
    #  pdb2_filename = struc2_allchains

    struc1_allchains = Protein(struc1_allchains)
    struc2_allchains = Protein(struc2_allchains)

    if subset1 is not None:
      subset1 = Protein(subset1)
    else:
      subset1 = struc1_allchains

    if subset2 is not None:
      subset2 = Protein(subset2)
    else:
      subset2 = struc2_allchains

    # Use only the first chain
    subset1 = subset1[0]
    subset2 = subset2[0]

    if normalise_by_first:
      options += " -L %d"%(len(subset1))

    # FORCE PRE-PARSING AND CREATION OF TEMPORARY FILES!!!
    # This ensures predictable behaviour with regards to insertion codes, etc., which TM-align just removes from input files.
    #
    # This will create temporary PDB files and run TMalign on those.
    if min(len(subset1), len(subset2)) < 3:
      raise StructureTooShortError("TMalign cannot deal with structures shorter than 3 residues. inputs: %s (%d residues) %s (%d residues)\n" % (struc1_allchains.code,len(subset1), struc2_allchains.code, len(subset2)))
    transform, alignment_info = tmalign_objects(subset1, subset2, options)


    ## Correct any sequence differences caused by TM-align's parsing of the PDB files
    #
    a = Ali(">seq1\n%s\n>seq2\n%s\n"%(alignment_info["seq1"], alignment_info["seq2"]), fasta_mode=True)
    #print "Before correction:"
    #print a[0].master
    #print a[1].master
    a.add(Ali(">seq1\n%s\n"%(subset1.get_seq()), fasta_mode=True), merge_duplicates=True)
    a.add(Ali(">seq2\n%s\n"%(subset2.get_seq()), fasta_mode=True), merge_duplicates=True)
    for i in range(2):
      try:
        altseq = a[i]["alternative sequence 1"].seq
        a[i]["alternative sequence 1"].seq = a[i].master.seq
        a[i].master.seq = altseq
      except Ali.InvalidEntryError:
        pass
    #print "After correction:"
    #print a[0].master
    #print a[1].master
    #print
    alignment_info["seq1"] = a[0].master.seq
    alignment_info["seq2"] = a[1].master.seq
    #
    ##


    if modify_structures:
      if not transform:
        raise PoorSuperpositionError("Poor superposition. TM-align did not generate a rotation matrix.")
      transform_structure(struc1_allchains, transform)

    #rmsd_value = pdb3dsuperimpose(struc1_allchains, struc2_allchains, seq1, seq2, subset1, subset2, align_atoms, modify_structures)

    if normalise_by_first:
      alignment_info["TM-score"] = alignment_info["TM-score (Chain_1)"]

    if not return_transform:
      return alignment_info["seq1"], alignment_info["seq2"], alignment_info
    return alignment_info["seq1"], alignment_info["seq2"], alignment_info, transform

