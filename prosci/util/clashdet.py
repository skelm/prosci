#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import re
import subprocess
import tempfile
import collections
from prosci.util.protein import Protein, ResidueList, Pdb, Atom

re_mainchainWithoutO = re.compile("atom1:(N|CA|C) atom2:(N|CA|C) ")
re_mainchainWithCB = re.compile("atom1:(N|CA|C|O|CB) atom2:(N|CA|C|O|CB) ")
re_mainchainWithCB2other = re.compile("atom[12]:(N|CA|C|O|CB) ")

re_atom_types = re.compile(r"atom1:(\w+) atom2:(\w+) ")

_DEFAULT_STRINGENCY = 0.63 # cut-offs similar to PyMOL


def delete_clashing_sidechains(decoy, stringency=_DEFAULT_STRINGENCY):
  decoy = Protein(decoy)
  clashes = get_clashes(decoy, stringency)
  for atoms in clashes:
    for a in atoms:
      if a.atom not in ("N", "CA", "C", "O", "CB"):
        i_c, i_r = decoy.find_residue(a)
        decoy[i_c][i_r].rest = []
  return clashes


def get_clashes(decoy, stringency=_DEFAULT_STRINGENCY):
  "Get clashes in the given structure, indexed by atom type"
  if isinstance(decoy, str) and "\n" not in decoy:
    p = subprocess.Popen(["clashdet", "-s", str(stringency), decoy], stdout=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate()
  else:
    p = subprocess.Popen(["clashdet", "-s", str(stringency), "-"], stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate(str(decoy))

  lines = collections.deque(out.splitlines())
  clashes = []
  while lines:
    line = lines.popleft()
    if line.startswith("Clash:"):
      a1 = lines.popleft()
      a2 = lines.popleft()
      clashes.append([Atom(a1), Atom(a2)])
  return clashes


def get_clash_dict(decoy):
  "Get clashes in the given structure, indexed by atom type"
  if isinstance(decoy, str) and "\n" not in decoy:
    p = subprocess.Popen(["clashdet", decoy], stdout=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate()
  else:
    p = subprocess.Popen(["clashdet", "-"], stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate(str(decoy))

  clashes = {}
  for line in out.splitlines():
    if line.startswith("Clash:"):
      m = re_atom_types.search(line)
      atype1 = m.group(1)
      atype2 = m.group(2)
      key = [atype1, atype2]
      key.sort()
      key = " ".join(key)
      try:
        clashes[key] += 1
      except KeyError:
        clashes[key] = 1
  return clashes


def count_stratified_clashes(decoy):
  "Count clashes in the given structure, return counts for (N_CA_C, N_CA_C_O, N_CA_C_O_CB, N_CA_C_O_CB_2_other, other_2_other)"

  clashdict = get_clash_dict(decoy)

  N_CA_C = 0
  N_CA_C_O = 0
  N_CA_C_O_CB = 0
  N_CA_C_O_CB_2_other = 0
  other_2_other = 0
  for key in clashdict:
    a, b = key.split(" ")
    if a in ("N", "CA", "C", "O", "CB"):
      if b in ("N", "CA", "C", "O", "CB"):
        if "CB" not in (a, b):
          if "O" not in (a, b):
            N_CA_C += 1
          else:
            N_CA_C_O += 1
        else:
          N_CA_C_O_CB += 1
      else:
        N_CA_C_O_CB_2_other += 1
    elif b in ("N", "CA", "C", "O", "CB"):
        N_CA_C_O_CB_2_other += 1
    else:
        other_2_other += 1
  N_CA_C /= 2
  N_CA_C_O /= 2
  N_CA_C_O_CB /= 2
  N_CA_C_O_CB_2_other /= 2
  other_2_other /= 2

  # Make the clash counts cumulative
  #
  result = [N_CA_C, N_CA_C_O, N_CA_C_O_CB, N_CA_C_O_CB_2_other, other_2_other]
  for i in range(1, len(result)):
    result[i] += result[i-1]

  return result


def count_clashes(decoy, stringency=_DEFAULT_STRINGENCY, mode="all", boolean=False):
    "Check if there is at least one clash (between only the specified atom types) in the given structure"
    assert mode in ("all", "CB", "backbone")
    if isinstance(decoy, str):
        if "\n" not in decoy:
            decoy = ResidueList(decoy)
        else:
            decoy = ResidueList(Pdb(decoy, nofilter=True))
    elif isinstance(decoy, Protein):
        decoy = decoy.to_residuelist()
    elif not isinstance(decoy, ResidueList):
        decoy = ResidueList(decoy)

    data = []
    if mode == "CB":
      for res in decoy:
        for a in (res.N, res.CA, res.C, res.O, res.CB):
          if a is not None:
            data.append(str(a))
    elif mode == "backbone":
      for res in decoy:
        for a in res.iter_backbone():
          data.append(str(a))
    else: # all
      for res in decoy:
        for a in res:
          data.append(str(a))

    if boolean:
      p = subprocess.Popen(["clashdet", "-s", str(stringency), "-cb", "-"], stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)
    else:
      p = subprocess.Popen(["clashdet", "-s", str(stringency), "-c", "-"], stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate("".join(data))
    for line in out.splitlines():
        if line.startswith("Total clashes:"):
          return int(line.split(":")[-1].strip())
    raise RuntimeError("Failed to parse clashdet output")


def has_clash(decoy, stringency=_DEFAULT_STRINGENCY, mode="all"):
  "Check if there is at least one clash (between only the specified atom types) in the given structure"
  return count_clashes(decoy, stringency, mode, True)


def has_backbone_or_CB_clash(decoy, stringency=_DEFAULT_STRINGENCY, boolean=True):
    "Check if there is at least one clash between backbone or CB atoms"
    return has_clash(decoy, stringency, "CB", boolean)


def has_backbone_clash(decoy, stringency=_DEFAULT_STRINGENCY, boolean=True):
    "Check if there is at least one clash between backbone or CB atoms"
    return has_clash(decoy, stringency, "backbone", boolean)


def count_inter_structure_clashes(decoy1, decoy2, stringency=_DEFAULT_STRINGENCY, mode="all"):
    "Count the clashes between decoy1 and decoy2"
    clashes1 = has_clash(decoy1, stringency, mode, False)
    clashes2 = has_clash(decoy2, stringency, mode, False)
    clashes_combined = has_clash(decoy1+decoy2, stringency, mode, False)
    return clashes_combined - (clashes1 + clashes2)

