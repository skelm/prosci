#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Utility functions for using the prosci.util library from within PyMOL
#
# Author: Sebastian Kelm
# Created: 29/08/2017
#
from __future__ import print_function
import numpy

from prosci.util.protein import Protein, ResidueList, Pdb, Atom, residueLetter, residueCode
from prosci.util.psa import annotate_surface_accessibility, get_surface_accessibility_dict

# Properties of a chempy.Atom:
['alt', 'atom_properties', 'b', 'chain', 'color_code', 'coord', 'custom', 'elec_radius', 'flags', 'formal_charge', 'get_free_valence', 'get_implicit_valence', 'get_mass', 'get_number', 'get_signature', 'has', 'hetatm', 'id', 'in_same_residue', 'index', 'name', 'new_in_residue', 'numeric_type', 'partial_charge', 'properties', 'q', 'resi', 'resi_number', 'resn', 'resn_code', 'segi', 'ss', 'stereo', 'symbol', 'text_type', 'u_aniso', 'vdw']


def atom_chempy_to_prosci(ca):
  pa = Atom()
  pa.hetatm    = bool(ca.hetatm)
  pa.iatom     = ca.id
  pa.atom      = ca.name
  pa.altloc    = ca.alt
  pa.res       = ca.resn
  pa.chain     = ca.chain
  pa.ires      = ca.resi_number
  pa.inscode   = ca.resi[-1:] if ca.resi[-1:].isalpha() else ''
  pa.xyz       = numpy.array(ca.coord)
  pa.occup     = ca.q
  pa.b         = ca.b
  pa.element   = ca.symbol
  #pa.charge    = ca.partial_charge
  return pa


def model_to_protein(model):
  atoms = []
  for a in model.atom:
    atoms.append(atom_chempy_to_prosci(a))
  return Protein(atoms)


def model_surface_accessibility(model):
  return annotate_surface_accessibility(model_to_protein(model))


def model_surface_accessibility_dict(model, bcutoff=0.1):
  return get_surface_accessibility_dict(model_to_protein(model), bcutoff)


# The below funcitons only work when executed via the "run" command from within
# PyMOL, not via an import, unless you've got PyMOL installed as a library.
#
try:
  from .pymol import cmd
except ImportError:
  pass
else:
  def _get_model(sele):
    return cmd.get_model(sele)

  def selection_to_protein(sele):
    return model_to_protein(_get_model(sele))

  def selection_surface_accessibility(sele):
    return model_surface_accessibility(_get_model(sele))

  def select_residues_with_surface_accessible_sidechains(sele, name="sele", cutoff=0.05):
    p = selection_surface_accessibility(sele)
    cmd.select(name, "not all")
    for chain in p:
      for res in chain:
        if res.access_sidechain > cutoff:
          cmd.select(name, "(%s) or (chain %s and resi %d%s and resn %s)"%(name, res.chain, res.ires, res.inscode, res.res))



