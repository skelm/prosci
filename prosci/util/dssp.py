#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DSSP output parser
# by Sebastian Kelm, 2014
#
# Losely based on the DSSP parsing code found here: http://openwetware.org/wiki/Wilke:ParseDSSP
# and adapted to fit into Sebastian Kelm's prosci.util library.
#
from __future__ import print_function


import sys
import os
import shutil
import tempfile
import subprocess

from prosci.util.ali import Ali
from prosci.util.protein import Protein, ResidueList
from prosci.util.pdb3d import dist

# Taken from:
# http://prowl.rockefeller.edu/aainfo/volume.htm
# C. Chotia, The Nature of the Accessible and Buried Surfaces in Proteins, J. Mol. Biol., 105(1975)1-14
# Residue volume in Gly-X-Gly
"""
  A 115
  C 135
  D 150
  E 190
  F 210
  G 75
  H 195
  I 175
  K 200
  L 170
  M 185
  N 160
  P 145
  Q 180
  R 225
  S 115
  T 140
  V 155
  W 255
  Y 230
"""

# Taken from:
# http://peds.oxfordjournals.org/content/15/8/659.full.pdf
# Residue volume in Gly-X-Gly
"""
  A 116
  C 141
  D 155
  E 187
  F 223
  G 84
  H 199
  I 190
  K 207
  L 198
  M 211
  N 168
  P 145
  Q 189
  R 249
  S 126
  T 148
  V 162
  W 265
  Y 238
"""

# Taken from:
# http://peds.oxfordjournals.org/content/15/8/659.full.pdf
# Residue volume in Ala-X-Ala
"""
  A 103
  C 128
  D 142
  E 173
  F 210
  G 70
  H 185
  I 176
  K 194
  L 184
  M 197
  N 155
  P 127
  Q 175
  R 235
  S 112
  T 134
  V 149
  W 252
  Y 225
"""

RESIDUE_SURFACE = {}
for line in """
  A 115
  C 135
  D 150
  E 190
  F 210
  G 75
  H 195
  I 175
  K 200
  L 170
  M 185
  N 160
  P 145
  Q 180
  R 225
  S 115
  T 140
  V 155
  W 255
  Y 230
  """.strip().splitlines():
    res, surface = line.split()
    RESIDUE_SURFACE[res] = float(surface)


ACCESSIBILITY_CUTOFF = 0.07 # this is a percentage of a residue's surface


def annotate_single_chain(struc, return_fractions=False):
  "Generate an Ali object containing the annotation for the given structure, treated as a single chain"
  annotator = DSSPAnnotator(struc)
  annot, acc_fractions = annotator.get_annotation_strings(True)

  code = struc.code.strip()
  if not code:
    code = "structure"

  a = ">%s\n%s\n%s*\n" % (code, "structure", annotator.struc.get_seq())
  for k in annot:
    a += ">%s\n%s\n%s*\n" % (code, k, annot[k])
  try:
    if return_fractions:
      return Ali(a), acc_fractions
    return Ali(a)
  except:
    print(a, file=sys.stderr)
    raise

def annotate_protein(struc):
  if not isinstance(struc, Protein):
    struc = Protein(struc)

  annot = annotate_single_chain(struc)

  seq = annot[0].master.seq
  try:
    protein_indices = struc.map_to_seq(seq)
  except:
    print("Structure:", file=sys.stderr)
    print(struc.get_seq(), file=sys.stderr)
    print("Annotation:", file=sys.stderr)
    print(annot, file=sys.stderr)
    raise

  output = []
  for chain, chain_indices in zip(struc, protein_indices):
    a = annot.copy_columns(chain_indices)
    a[0].code = struc.code + chain.chain
    output.append(a[0])

  return Ali(output)


def disulphide_in_range(res1, res2, verbose=False):
  """Check the two given cystein residues are close enough to potentially be disulphide-bonded."""
  if res1.res != "CYS" or res2.res != "CYS":
    return False

  if res1.is_left_neighbour_of(res2) or res2.is_left_neighbour_of(res1):
    # Neighbouring residues don't usually bond and we don't want to refine
    # mainchain atoms unless we really have to
    return False

  try:
    a1 = res1.get_atom("CA")
    a2 = res2.get_atom("CA")
    d = dist(a1, a2)
    if d <= 0.5 or d > 8.0:
      #if verbose:
      #  print >>sys.stderr, "CYS CA is too close or too far. Don't refine: %.2f" % (d)
      return False
  except ValueError:
    if verbose:
      print("CA not found in CYS residue: %s, %s" % (res1.get_id(), res2.get_id()), file=sys.stderr)
    return False

  try:
    a1 = res1.get_atom("CB")
    a2 = res2.get_atom("CB")
    d = dist(a1, a2)
    if d <= 0.5 or d > 8.0:
      if verbose:
        print("CYS CB too close or too far (%.2f): %s, %s" % (d, res1.get_id(), res2.get_id()), file=sys.stderr)
      return False
  except ValueError:
    if verbose:
      print("CB not found in CYS residue, but close enough: %s, %s" % (res1.get_id(), res2.get_id()), file=sys.stderr)
    return True
  return True


def disulphide_is_bonded(res1, res2):
  """Check the two given cystein resides' SG atoms are close enough to have an existing disulphide-bond."""

  try:
    a1 = res1.get_atom("SG")
    a2 = res2.get_atom("SG")
    d = dist(a1, a2)
    if d > 1.8 and d <= 2.2:
      return True
  except ValueError:
    pass
  return False


class DSSPAnnotator(object):
  "Annotates a protein structure with DSSP"


  def __init__(self, struc):
    if not isinstance(struc, ResidueList):
      if isinstance(struc, Protein):
        struc = struc.to_residuelist()
      else:
        struc = ResidueList(struc)
    self.struc = struc
    self.data = self._annotate()


  def _annotate(self):
    strucfname = "struc.pdb"
    tmpdir = tempfile.mkdtemp()
    try:
      with open(os.path.join(tmpdir, strucfname), "w") as f:
        f.write(str(self.struc))

      p = subprocess.Popen(["dssp", strucfname], cwd=tmpdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
      out, err = p.communicate()

      data = DSSPData(out)

      #if err.strip():
      #  sys.stderr.write("DSSP errors:\n%s\n\n" % (err))
    finally:
      shutil.rmtree(tmpdir)

    return data


  def get_secondary_structure_string(self):
    """
    B = residue in isolated beta-bridge
    E = extended strand, in beta-ladder
    G = 3-helix (3/10 helix)
    H = alpha-helix
    I = 5-helix (pi helix)
    T = hydrogen bonded turn
    S = bend
    """
    d = self.data
    struc = self.struc

    n = 0
    output = ""
    for i, (ires, inscode, chain, aa, sec_struc) in enumerate(zip(d.resnum, d.inscode, d.chain, d.aa, d.struct)):
      if aa == "!*":
        continue
      S = sec_struc[0]
      res = self.struc[n]
      assert res.ires == int(ires)
      assert res.inscode == inscode
      assert res.chain == chain
      output += S
      n += 1

    assert len(output) == len(struc)
    return output


  def get_annotation_strings(self, return_fractions=False):
    d = self.data
    struc = self.struc

    n = 0
    sstruc = ""
    access = ""
    access_fractions = []
    for i, (ires, inscode, chain, aa, sec_struc, acc) in enumerate(zip(d.resnum, d.inscode, d.chain, d.aa, d.struct, d.acc)):
      if "!" in aa:
        continue
      S = sec_struc[0]
      res = self.struc[n]
      n += 1

      #assert str(res.ires) == ires, "'%d' != '%s', '%s', '%s', '%s', '%s', '%s'"%(res.ires, ires, inscode, chain, aa, sec_struc, acc)
      #assert res.inscode == inscode, "%s %s %s %s %s %s"%(ires, inscode, chain, aa, sec_struc, acc)
      #assert res.chain == chain, "%s %s %s %s %s %s"%(ires, inscode, chain, aa, sec_struc, acc)

      while not (str(res.ires) == ires and res.inscode == inscode and res.chain == chain):
        # We have a residue that DSSP did not annotate. Generate default annotation until we reach annotated residues again.
        #
        sstruc += "C"
        access += "T"
        access_fractions.append(0.3)
        res = self.struc[n]
        n += 1

      sstruc += S

      try:
        acc = float(acc) / RESIDUE_SURFACE[aa]
      except KeyError:
        acc = float(acc) / RESIDUE_SURFACE["A"]

      isacc = "T"
      if acc < ACCESSIBILITY_CUTOFF:
        isacc = "F"

      access += isacc
      access_fractions.append(acc)

    while len(sstruc) < len(struc):
      sstruc += "C"
    while len(access) < len(struc):
      access += "T"
      access_fractions.append(0.3)

    assert len(sstruc) == len(struc), "sstruc=%s struc=%s" % (len(sstruc), len(struc))
    assert len(access) == len(struc), "access=%s struc=%s" % (len(access), len(struc))
    assert len(access_fractions) == len(struc), "access_fractions=%s struc=%s" % (len(access_fractions), len(struc))

    simplestruc = []
    for x in sstruc:
      if x in "HGI":
        simplestruc += "H"
      elif x in "E":
        simplestruc += "E"
      else:
        simplestruc += "C"
    simplestruc = "".join(simplestruc)

    dihedrals = struc.get_backbone_dihedrals()
    joy_sstruc = []
    for x, dh in zip(simplestruc, dihedrals):
      if x == "C" and (dh[0] is not None and dh[0] >= 0):
        x = "P"
      joy_sstruc.append(x)
    joy_sstruc = "".join(joy_sstruc)

    disulphide = ["F"]*len(struc)
    for i, res1 in enumerate(struc):
      if res1.res != "CYS":
        continue
      for j in range(i+1, len(struc)):
        res2 = struc[j]
        if disulphide_in_range(res1, res2) and disulphide_is_bonded(res1, res2):
          disulphide[i] = disulphide[j] = "T"
    disulphide = "".join(disulphide)

    if return_fractions:
      return {"DSSP":sstruc.replace(" ", "C"), "surface accessibility":access, "secondary structure":simplestruc, "secondary structure and phi angle":joy_sstruc, "disulphide":disulphide}, access_fractions
    return {"DSSP":sstruc.replace(" ", "C"), "surface accessibility":access, "secondary structure":simplestruc, "secondary structure     and phi angle":joy_sstruc, "disulphide":disulphide}




################################################################################
# DSSP OUTPUT PARSER
#
# The below code was adapted from http://openwetware.org/wiki/Wilke:ParseDSSP
# and modified by Sebastian Kelm, 7 March 2014.
################################################################################

class DSSPData(object):
  def __init__(self, txt):
    self.num    = []
    self.resnum = []
    self.inscode= []
    self.chain  = []

    self.aa     = []

    self.struct = []

    self.bp1    = []
    self.bp2    = []
    self.blabel = []

    self.acc    = []

    self.h_nho1 = []
    self.h_ohn1 = []
    self.h_nho2 = []
    self.h_ohn2 = []

    self.tco    = []
    self.kappa  = []
    self.alpha  = []
    self.phi    = []
    self.psi    = []
    self.xca    = []
    self.yca    = []
    self.zca    = []

    line_num = 0
    start=False
    for line in txt.splitlines():
      if '#' in line:
        start=True
        continue
      if start:
        self.num.append(    int(line[0:5].strip()) )
        self.resnum.append( line[5:10].strip() )
        self.inscode.append(line[10:11].strip() )
        self.chain.append(  line[11:12].strip() )

        self.aa.append(     line[13:15].strip() )

        self.struct.append( line[16:25] )

        self.bp1.append(    line[25:29].strip() )
        self.bp2.append(    line[29:33].strip() )
        self.blabel.append( line[33:34].strip() )

        self.acc.append(    line[34:38].strip() )

        self.h_nho1.append( line[38:50].strip() )
        self.h_ohn1.append( line[50:61].strip() )
        self.h_nho2.append( line[61:72].strip() )
        self.h_ohn2.append( line[72:83].strip() )

        self.tco.append(    line[83:91].strip() )
        self.kappa.append(  line[91:97].strip() )
        self.alpha.append(  line[97:103].strip() )
        self.phi.append(    line[103:109].strip() )
        self.psi.append(    line[109:115].strip() )
        self.xca.append(    line[115:122].strip() )
        self.yca.append(    line[122:129].strip() )
        self.zca.append(    line[129:136].strip() )




if __name__ == "__main__":
  from glob import glob

  for fname in glob("*.pdb"):
    p = Protein(fname)
    print(annotate_single_chain(p))
    #print annotate_protein(p)

