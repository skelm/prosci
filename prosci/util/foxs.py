#!/usr/bin/env python
from __future__ import print_function
import os
import tempfile
import subprocess
import shutil
import re

re_Chi = re.compile(r"\sChi\s*=\s*([0-9]+\.?[0-9]*)")

def score_decoy_fit_to_saxs(pdbfile, saxs_profile, fixed_addition=None, cg=False):
  "Score a given conformation using the FoXS program and return the Chi value"

  if fixed_addition is not None:
    pdbfile = merge_align_shared_chains(pdbfile, fixed_addition)

  write_temp = (not isinstance(pdbfile, str)) or ("\n" in pdbfile)
  if write_temp:
    tmpdir = tempfile.mkdtemp()
    fname = os.path.join(tmpdir, "conf.pdb")
    with open(fname, "w") as f:
      f.write(str(pdbfile))
    pdbfile = fname

  try:
    if cg:
      # only look at CA atoms for faster computation
      p = subprocess.Popen(["foxs", "-r", pdbfile, saxs_profile], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    else:
      # use all atoms
      p = subprocess.Popen(["foxs", pdbfile, saxs_profile], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate()
    chi = -1.0
    for line in out.splitlines():
      m = re_Chi.search(line)
      if m:
        chi = float(m.group(1))
        break
  finally:
    if write_temp:
      shutil.rmtree(tmpdir)

  return chi


def fix_saxs_profile_negative_intensities(saxs_profile_in, saxs_profile_out):
  with open(saxs_profile_in) as fin, open(saxs_profile_out, "w") as fout:
    for line in fin:
      line = line.rstrip()
      if not line.startswith("REMARK") and not line.startswith("#"):
        try:
          fields = line.split()
          intensity = float(fields[1])
          error = float(fields[2])
          if intensity - error < 0:
            intensity = 0.0
          line = "%s\t%g\t%g" % (fields[0], intensity, error)
        except ValueError:
          pass
      fout.write(line+"\n")


def merge_align_shared_chains(frame, fixed_addition):
  "Merge two Protein objects by aligning their shared chains. Keep all unique chains. Of the shared chains, keep only the copy from the first argument. The second argument is used as the reference coordinate system."

  if fixed_addition is None:
    return frame

  from prosci.util.protein import Protein
  from prosci.util.tmalign import superimpose

  if not isinstance(frame, Protein):
    frame = Protein(frame)
  if not isinstance(fixed_addition, Protein):
    fixed_addition = Protein(fixed_addition)

  cc1 = set([c.chain for c in frame])
  cc2 = set([c.chain for c in fixed_addition])
  shared_chains = sorted(cc1 & cc2)
  assert shared_chains

  subset1 = Protein([])
  subset2 = Protein([])
  for c in shared_chains:
    subset1.append(frame.get_chain(c))
    subset2.append(fixed_addition.get_chain(c))

  ## WARNING: The superimpose function will only align the first shared chain...
  if len(shared_chains) > 1:
    print("merge_align_shared_chains: Aligning only the first chain (%s) out of %d shared chains" % (shared_chains[0], len(shared_chains)), file=sys.stderr)

  info = superimpose(frame, fixed_addition, subset1, subset2)[-1]
  assert info["TM-score"] >= 0.5

  # Add unique chains from fixed_addition to frame
  #
  cc_unique_fixed = sorted(cc2 - set(shared_chains))
  for c in cc_unique_fixed:
    frame.append(fixed_addition.get_chain(c))

  #print "Chains in merged file:", [c.chain for c in frame]

  return frame

