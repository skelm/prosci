#!/usr/bin/env python
from __future__ import print_function
import os
import shutil
import tempfile

#from prosci.common import run_systemcall
from prosci.util.command import Command

DEBUG = False

class TempDir(object):
  def __init__(self, path=None, keep=False):
    self.delete_on_exit = not keep
    if path is None:
      self.path = tempfile.mkdtemp()
    else:
      self.path = os.path.abspath(path)
      if not os.path.exists(self.path):
        os.mkdir(self.path)
      else:
        # If the given path already exists, don't auto-delete it no matter what
        self.delete_on_exit = False
  
  def __enter__(self):
    if DEBUG: print("TempDir.__enter__")
    return self

  def __exit__(self, *args):
    if DEBUG: print("TempDir.__exit__")
    if self.delete_on_exit:
      self.delete()
  
  def __del__(self):
    if DEBUG: print("TempDir.__del__")
    if self.delete_on_exit:
      self.delete()
  
  def __repr__(self):
    return "TempDir(%s, %s)" % (repr(self.path), repr(self.delete_on_exit))
  
  
  def delete(self):
    try:
      if DEBUG: print("TempDir.delete")
      shutil.rmtree(self.path)
    except OSError as e:
      pass
  
  def move(self, path):
    shutil.move(self.path, path)
  
  
  def get_path(self, path):
    return os.path.join(self.path, path)
  
  def write_file(self, name, content):
    with open(self.get_path(name), "wb") as f:
      f.write(content)
  
  def read_file(self, name):
    with open(self.get_path(name), "rb") as f:
      return f.read()
  
  def delete_file(self, name):
    os.remove(self.get_path(name))
  
  def move_file(self, name, path):
    shutil.move(self.get_path(name), path)

  def copy_file(self, name, path):
    shutil.copy2(self.get_path(name), path)
  
  def open(self, name, mode="rb"):
    return open(self.get_path(name), mode)
  
  
  def run_command(self, command, timeout=999999999, **kwargs):
    c = Command(command)
    returncode = c.run(timeout, cwd=self.path, **kwargs)
    return c.process
  
  
  def run_systemcall(self, command, **kwargs):
    return self.run_command(command, shell=True, **kwargs)
  
  


if __name__ == "__main__":
  import random
  DEBUG = True
  numbers = list(range(1, 9999999))
  random.shuffle(numbers)
  n = 0.0
  with TempDir() as td:
    print("Beginning of with block")
    while len(numbers) >= 2:
      n += numbers.pop() / numbers.pop()
    print(n)
    print("End of with block")
  print("After with block")
  numbers = list(range(1, 9999999))
  random.shuffle(numbers)
  p = 0.0
  while len(numbers) >= 2:
    p += numbers.pop() / numbers.pop()
  print(p)
  print("End")

