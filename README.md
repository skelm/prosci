# README #

The prosci python library.

Copyright by Sebastian Kelm, 2008-2017.

First developed as part of iMembrane and MEDELLER, copyright by Sebastian Kelm, Jiye Shi, Charlotte M. Deane, 2008-2010.

Available at [bitbucket.org](https://bitbucket.org/skelm/prosci) under the [GNU Lesser General Public Licence (LGPL)](https://www.gnu.org/licenses/lgpl.html).

### Purpose ###

* Prosci stands for protein science and is a general purpose utility library for protein structural bioinformatics applications.
* This library underlies all the scientific python tools written by Sebastian Kelm, including most of the MEMOIR suite of membrane protein modelling tools (MEDELLER, iMembrane, PyFREAD, Completionist).
* Most of the code is extremely reusable and is a collection of interfaces to other command line tools, as well as parsers for PDB, FASTA and PIR format files.
* The prosci.common library contains a bunch of commonly useful utility functions, including "parallelise()" which makes it extremely easy to run a function in parallel without any extra coding.
* What's not included is the main code for MEDELLER and the other tools. This may added in the future, if the respective co-authors agree. Currently this code is only available upon request.

### How do I get set up? ###

* Just download the code and put it somewhere in your PYTHONPATH.
* The author recommends using the Anaconda python distribution, which includes all the required libraries to run prosci.
* Most of the code is documented via inline comments.
* In case of issues, please use the issue tracker.

### Contribution guidelines ###

* If you are planning to contribute code, please provide examples to run on. Ideally, write tests into the libraries themselves using nosetests (make functions with names that start with "test\_" which include a bunch of assert statements).

### Who do I talk to? ###

* For questions about the code itself, contact Sebastian Kelm (kelm at stats.ox.ac.uk)
* For questions regarding licencing or the public web servers using this code, contact the Oxford Protein Informatics Group (opig at stats.ox.ac.uk)

### Licence ###

* The prosci python library is made available under the GNU Lesser General Public Licence (LGPL). For details see: https://www.gnu.org/licenses/lgpl.html
* The author asks that any improvements or bugfixes made are fed back to the author, either by a pull request on Bitbucket or by directly contacting the author.
* You may use this library as you see fit, but the author would appreciate being informed of any major projects that use his work.
* If you write bioinformatics tools related to protein 3D modelling, please consider citing this paper:
  * Bioinformatics, Volume 26, Issue 22, 15 November 2010, Pages 2833-2840, https://doi.org/10.1093/bioinformatics/btq554
